//
//  MenuItem.swift
//  RNIS
//
//  Created by Артем Кулагин on 01.09.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

/**
 Класс ячейки бокового меню
 */
class MenuItem: Hashable {
    var hashValue: Int {
        return (imagePassive?.hashValue ?? 0) + (imageActive?.hashValue ?? 0) + (vc?.hashValue ?? 0)
    }
    
    static func == (lhs: MenuItem, rhs: MenuItem) -> Bool {
        return lhs.hashValue == rhs.hashValue
    }
  
    
    /// корневой навигатор
    var vc: BaseNavigationController?
    
    /// Изображение
    var imagePassive: UIImage?
    var imageActive: UIImage?
    
    /// Метод инициализации 
    init(_ imageActive: UIImage?, _ imagePassive: UIImage?, _ rootViewController: UIViewController?) {
        self.imageActive = imageActive
        self.imagePassive = imagePassive
        if let rootViewController = rootViewController?.redContainer {
            self.vc = BaseNavigationController(rootViewController: rootViewController)
        }
    }
    
    /// Метод появления
    func show() {
        RNSMenuManager.showItem(self)
    }
    
    func toRoot() {
        vc?.popToRoot()
    }
}
