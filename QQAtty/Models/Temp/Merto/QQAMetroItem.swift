//
//  QQAMetroIItem.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAMetroItem {
    
    var id: String?
    var name: String?
    
    init(_ name: String?) {
        self.name = name
    }
}
