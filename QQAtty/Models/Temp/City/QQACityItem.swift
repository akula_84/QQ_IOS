//
//  QQACityItem.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQACityItem {
    var id: String?
    var name: String?
    
    init(_ name: String?) {
        self.name = name
    }
}
