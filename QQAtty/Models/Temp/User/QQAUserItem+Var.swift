//
//  QQAUserItem+Var.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQAUserItem {
    var isEmptyImage: Bool {
        return avatar?.isEmpty ?? true
    }
}
