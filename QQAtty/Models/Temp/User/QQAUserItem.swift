//
//  QQAUserItem.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAUserItem {
    var id: String?
    var idCity: String?
    var сityName: String?
    var firstName: String?
    var lastName: String?
    var phone: String?
    var email: String?
    var avatar: String?
    var token: String?
    
    
    func prepareCity(_ item: QQACityItem?) {
        guard let item = item else {
            return
        }
        idCity = item.id
        сityName = item.name
    }
}
