//
//  QQAUserItem+Stub.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQAUserItem {
    static var stub: QQAUserItem {
        let item = QQAUserItem()
        item.сityName = "Москва"
        item.firstName = "Иван"
        item.lastName = "Константинов"
        item.phone = "+79154568024"
        item.email = "konstantinov@ya.ru"
        return item
    }
}
