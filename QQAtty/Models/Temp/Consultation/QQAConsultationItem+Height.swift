//
//  QQAConsultationItem+Height.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQAConsultationItem {
    
    var height: CGFloat {
        var heigth = CGFloat(88)
        heigth += heightAddress
        heigth += 50
        return heigth
    }
    
    var heightAddress: CGFloat {
        return address?.height(.system15, width: widthScreen) ?? 0
    }
    
    var widthScreen: CGFloat {
        return UIScreen.width - 37 - 37
    }
}
