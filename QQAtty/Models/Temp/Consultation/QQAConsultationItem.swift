//
//  QQAConsultationItem.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAConsultationItem {
    var time: Date?
    var address: String?
    var phone: String?
}
