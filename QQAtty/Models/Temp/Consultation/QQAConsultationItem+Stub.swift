//
//  QQAConsultationItem+Stub.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQAConsultationItem {
    
    static var stubs: [QQAConsultationItem] {
        let item1 = stub(time: Date(), address: "г. Москва, м. Александровский сад, Большой кисловодский пер.,1, \"БФ-юрист\"", phone: "89452022608")
        let item2 = stub(time: Date().appendDay(-1), address: "г. Москва, Сроки сдачи имущества", phone: "89452022608")
        let item3 = stub(time: Date().appendDay(-10), address: "г. Москва, ожет ли суд прекратить производство по делу", phone: "89452022608")
        return [item1,item2,item3]
    }
    
    static func stub(time: Date?, address: String?, phone: String?) -> QQAConsultationItem {
        let item = QQAConsultationItem()
        item.time = time
        item.address = address
        item.phone = phone
        return item
    }
}
