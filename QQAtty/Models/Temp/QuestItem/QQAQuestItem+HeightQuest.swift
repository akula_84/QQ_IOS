//
//  QQAQuestItem+Height.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation
import UIKit

extension QQAQuestItem {
    var heightQuest: CGFloat {
        var heigth = CGFloat(24)
        heigth += heightTitleQuest
        heigth += 35
        heigth += heightAttrDescriptionQuest
        heigth += 56
        return heigth
    }
    
    var widthScreenQuest: CGFloat {
        return UIScreen.width - 37 - 37
    }
    
    var heightTitleQuest: CGFloat {
        return heightTitleAt(widthScreenQuest)
    }

    var heightAttrDescriptionQuest: CGFloat {
        return heightDescriptionAt(widthScreenQuest)
    }
    
    func heightTitleAt(_ width: CGFloat) -> CGFloat {
        return title?.height(.sfMedium17, width: width) ?? 0
    }
    
    func heightDescriptionAt(_ width: CGFloat) -> CGFloat {
        return descriptionAttr?.height(width: width) ?? 0
    }
}
