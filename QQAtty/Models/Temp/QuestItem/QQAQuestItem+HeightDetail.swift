//
//  QQAQuestItem+HeightDetailswift.swift
//  QQAtty
//
//  Created by Артем Кулагин on 03.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQAQuestItem {
    
    var maxDetailHeight: CGFloat {
        return 278
    }
    
    var heightOpenButton: CGFloat {
        return 30
    }
    
    var heightDetail: CGFloat {
        var heigth = openDetail ? heightTextDetail : closeHeightDetail
        if isShowOpenButton {
            heigth += heightOpenButton
        }
        return heigth
    }
    
    var closeHeightDetail: CGFloat {
        return heightTextDetail > maxDetailHeight ? maxDetailHeight : heightTextDetail
    }
    
    var isShowOpenButton: Bool {
        return heightTextDetail > maxDetailHeight
    }
    
    var widthScreenDetail: CGFloat {
        return UIScreen.width - 35 - 35
    }
    
    var heightTitleDetail: CGFloat {
        return heightTitleAt(widthScreenDetail)
    }
    
    var heightAttrDescriptionDetail: CGFloat {
        return heightDescriptionAt(widthScreenDetail)
    }
    
    func calcHeightTextDetail() {
        var heigth = CGFloat(19)
        heigth += heightTitleDetail
        heigth += 19
        heigth += heightAttrDescriptionDetail
        heigth += 19
        self.heightTextDetail = heigth
    }
}
