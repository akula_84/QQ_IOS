//
//  QQAQuestItem+HeightComments.swift
//  QQAtty
//
//  Created by Артем Кулагин on 03.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQAQuestItem {
    
    var heightComments: CGFloat {
        var heigth = openDetail ? heightCellComments : closeHeightComments
        if isShowOpenButtonComments {
            heigth += heightOpenButton
        }
        return heigth
    }
    
    var closeHeightComments: CGFloat {
        return heightCellComments > maxDetailHeight ? maxDetailHeight : heightCellComments
    }
    
    var isShowOpenButtonComments: Bool {
        return heightCellComments > maxDetailHeight
    }
    
    var widthScreenComment: CGFloat {
        return UIScreen.width - 35 - 35
    }
    
    var heightAttrDescriptionComment: CGFloat {
        return heightDescriptionAt(widthScreenComment)
    }
    
    func calcHeightComments() {
        var heigth = CGFloat(94)
        heigth += heightAttrDescriptionComment
        heigth += 19
        self.heightCellComments = heigth
    }
}
