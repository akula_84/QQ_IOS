//
//  QQAQuestItem.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAQuestItem {
    
    var id: String?
    
    var title: String?
    var time: Date?
    var countAsk: Int?
    var isFavorite: Bool = false
    var descriptionAttr: NSAttributedString?
    var author: String?
    var city: String?
    
    var openDetail = false
    var heightTextDetail: CGFloat = 0
    var heightCellComments: CGFloat = 0
    
    var countAskText: String {
        guard let countAsk = countAsk else {
            return ""
        }
        var suffix = ""
        switch countAsk {
        case 0:
            suffix = "Нет ответов"
            break
        case 1:
            suffix = "\(countAsk) Ответ"
            break
        case 2,3,4:
            suffix = "\(countAsk) Ответа"
            break
        default:
            suffix = "\(countAsk) Ответов"
            break
        }
        return suffix
    }
    
    var dateText: String {
        guard let date = time else {
            return ""
        }
        let HHmm = date.stringHHmm
        if date.isToday {
            return date.hourBackText
        }
        
        if date.inYesterday {
            return "Вчера в \(HHmm)"
        }
        return date.stringDd_LLLL + " в " + HHmm
    }
    
    var isFavoriteImage: UIImage {
        return isFavorite ? #imageLiteral(resourceName: "favoriteFull") : #imageLiteral(resourceName: "favoriteClear")
    }
    
    func revertFavorites() {
        isFavorite = !isFavorite
    }
    
    func revertOpen() {
        openDetail = !openDetail
    }
}
