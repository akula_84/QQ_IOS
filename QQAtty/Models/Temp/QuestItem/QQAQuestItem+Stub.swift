//
//  QQAQuestItem+Stub.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQAQuestItem {

    static var stubText: String {
        return """
        <blockquote>Я не знаю, как в России, но в  Израиле каждый работающий ежемесячно перечисляет дополнительно деньги в разные страховые - пенсионные компании, которые по достижению пенсионного возраста, начинают платить как бы вторую дополнительную пенсию в дополнение к базисной, получаемой от государства. И чем больше были выплаты в такие пенсионные фонды, тем больше сумма такой второй пенсии.
        Конечно, есть и такие, которые ничего не платят, кроме основных взносов, которые  вздымаются государственной   страховой компанией, но они потом, выйдя на пенсию, получают минимальные выплаты.
        Так что тот, кто не хочет остаться с "голой жопой" в старости, вынужден думать об этом в молодости, ну а ток, кто об этом не думает и выйдя на пенсию начинает жаловаться, то таким хочется только напомнить басню про стрекозу.
        И ещё, есть одна хорошая поговорка:
        "Хороший хозяин  готовится к зиме летом" , ну а плохой - летом ни хрена не делает, а с приходом зимы,  винит во всём Путина с Медведева...
        <i class="text-small"><b>zamestitel</b></i> <i class="text-small">@ 2 Aug 2018 в 21:25</i></blockquote>
        
        Можете назвать какие страховые пенсионные компании в РФ через 20-40 лет продолжат своё существование и даже будут что-то там выплачивать на основании того, что они собрали с людей 20лет назад?
        """
    }
    
    static var detailText: String {
        return """
<div id="lipsum">
<p>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean consequat eu tellus sed porttitor. Maecenas efficitur consequat iaculis. Integer sed neque sem. Duis dapibus enim nisl, ac consequat massa iaculis lacinia. Integer ligula lorem, ultricies eget enim nec, egestas vestibulum urna. Cras condimentum, turpis ut mollis efficitur, tortor justo pellentesque turpis, et dignissim lacus est et elit. Curabitur tristique molestie quam, non mattis odio interdum ac. Phasellus nec tortor eu metus feugiat pellentesque eu congue massa. Morbi ipsum lorem, molestie nec fermentum non, laoreet ut odio. Aliquam metus justo, tincidunt ac accumsan nec, ornare nec lacus. Nam id est in enim euismod suscipit.
</p>
<p>
Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Cras dapibus neque vitae metus pulvinar, quis rhoncus lacus auctor. Mauris eu ipsum nisi. Donec urna odio, laoreet semper tellus at, ullamcorper tempor velit. In iaculis elit ligula, a ultrices enim egestas vitae. Vestibulum consectetur nisl urna, non convallis felis porttitor quis. Fusce nec porta augue. Morbi sed malesuada velit. Morbi bibendum pulvinar erat a consectetur. Nullam pulvinar vestibulum ex sed vestibulum. Nam scelerisque dui quis lacus convallis congue. Nulla eu sapien metus.
</p>
<p>
Nunc vulputate commodo felis non congue. Nam in molestie arcu. Phasellus eu viverra magna. Fusce convallis, enim at posuere viverra, ante nisl semper mauris, quis vehicula odio diam ut ligula. Mauris quis porttitor enim. Pellentesque fringilla ex vel orci porttitor, nec ornare nisi tempor. Sed tincidunt, quam sed condimentum fringilla, nibh est rutrum est, in tempor sem ligula vitae felis. Morbi quis scelerisque nulla. Donec aliquam tellus lobortis nisl faucibus, at eleifend nibh aliquet. Pellentesque vitae convallis elit, sit amet volutpat erat.
</p>
<p>
Etiam posuere massa eu sem suscipit, sit amet elementum mi semper. Ut mollis, turpis in egestas iaculis, augue erat auctor nunc, eget hendrerit erat nunc at tellus. Mauris porttitor nisi et quam porta elementum. Nam ac velit et lectus posuere tempor. Sed sagittis posuere turpis. Vestibulum varius condimentum nunc, vitae hendrerit nisl egestas vel. Nam a lorem sed ligula condimentum lacinia non sed turpis. In vulputate lectus ac mi ullamcorper, et laoreet purus auctor. Quisque finibus risus ut elit mollis convallis. Pellentesque vel libero ac neque dictum interdum.
</p>
<p>
Curabitur in maximus felis. Praesent mattis orci eu congue eleifend. Curabitur ultrices nisi sapien, a maximus eros tempor ac. Nam et egestas ligula, et semper ipsum. Sed ut imperdiet dui, vel faucibus metus. Proin non gravida libero. Donec auctor aliquam elit sed porta.
</p></div>
"""
    }
    
    
    static var stubs: [QQAQuestItem] {
        let item1 = stub(id: "0", title: "Сроки сдачи имущества", time: Date().appendHour(-3), description: "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?", countAsk: 25, isFavorite: false)
        
        let item2 = stub(id: "1", title: "Отношения между яндекс такси и партнером‐перевозчиком", time: Date().appendDay(-1), description: "Мы ООО. Работаем с яндекс такси как  агент. Нанимаем водителей через наше", countAsk: 2, isFavorite: false)
        
        let item3 = stub(id: "3", title: "Может ли суд прекратить производство по делу?", time: Date().appendDay(-5), description: "Добрый вечер. Сложилась следующая  ситуация. Между нами и ответчиком...", countAsk: 25, isFavorite: false)
        let item4 = stub(id: "0", title: "Сроки сдачи имущества", time: Date(), description: "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?", countAsk: 25, isFavorite: false)
        let item5 = stub(id: "0", title: "Сроки сдачи имущества", time: Date().appendHour(-1), description: "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?", countAsk: 0, isFavorite: false)
        let item6 = stub(id: "0", title: "Сроки сдачи имущества", time: Date().appendHour(-2), description: "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?", countAsk: 1, isFavorite: false)
        let item7 = stub(id: "0", title: "Сроки сдачи имущества", time: Date().appendHour(-4), description: "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?", countAsk: 3, isFavorite: false)
        let item8 = stub(id: "0", title: "Сроки сдачи имущества", time: Date().appendHour(-5), description: detailText, countAsk: 4, isFavorite: false)
        let item9 = stub(id: "0", title: "Сроки сдачи имущества", time: Date().appendHour(-6), description: stubText, countAsk: 5, isFavorite: false)
        
        return [item1,item2,item3,item4,item5,item6,item7,item8,item9]
    }
    
    static var stubsFavorite: [QQAQuestItem] {
        let item1 = stub(id: "0", title: "Валет Сроки сдачи имущества", time: Date().appendHour(-3), description: "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?", countAsk: 25, isFavorite: true)
        
        let item2 = stub(id: "1", title: "Туз Отношения между яндекс такси и партнером‐перевозчиком", time: Date().appendDay(-1), description: "Мы ООО. Работаем с яндекс такси как  агент. Нанимаем водителей через наше", countAsk: 2, isFavorite: true)
        
        let item3 = stub(id: "3", title: "Король Может ли суд прекратить производство по делу?", time: Date().appendDay(-5), description: "Добрый вечер. Сложилась следующая  ситуация. Между нами и ответчиком...", countAsk: 25, isFavorite: true)
        
        return [item1,item2,item3]
    }
    
    static var stubsMy: [QQAQuestItem] {
        let item1 = stub(id: "0", title: "Сроки сдачи имущества", time: Date().appendHour(-3), description: "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?", countAsk: 25, isFavorite: false)
        
        let item2 = stub(id: "1", title: "Отношения между яндекс такси и партнером‐перевозчиком", time: Date().appendDay(-1), description: "Мы ООО. Работаем с яндекс такси как  агент. Нанимаем водителей через наше", countAsk: 2, isFavorite: true)
        return [item1,item2]
    }
    
    static func stub(id: String?,
    title: String?,
    time: Date?,
    description: String?,
        countAsk: Int?, isFavorite: Bool) -> QQAQuestItem {
        let item = QQAQuestItem()
        item.id = id
        item.title = title
        item.time = time
        item.descriptionAttr = /*description?.html2Attributed*/ description?.stubAttr
        item.countAsk = countAsk
        item.isFavorite = isFavorite
        return item
    }
    
    static var detailItem: QQAQuestItem {
        let item = QQAQuestItem()
        item.id = "id"
        item.title = "Может ли суд прекратить производство по делу?"
        item.time = Date().appendHour(-3)
       // item.descriptionAttr = "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?".stubAttr
        
        item.descriptionAttr = (detailText + stubText + detailText + stubText).stubAttr
        item.countAsk = 2
        item.isFavorite = false
        item.author = "Иван Александров"
        item.city = "Москва"
        item.calcHeightTextDetail()
        return item
    }
    
    static var detailItemComments: [QQAQuestItem] {
        let item1 = detailItem
        item1.author = "Роман Новиков"
        let item2 = detailItem
        item2.descriptionAttr = stubText.stubAttr
        item2.author = "Александр Безгородов"
        let item3 = detailItem
        item3.author = "Степа Безгородов"
        item3.descriptionAttr = "Добрый вечер, подскажите пожалўиста через сколько должны съехать бывшие владельцы квартиры?".stubAttr
        let array = [item1,item2,item3]
        array.forEach {
            $0.calcHeightComments()
        }
        return array
    }
    
}
