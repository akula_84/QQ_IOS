//
//  QQAProfileViewController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAProfileViewController: UIViewController {
    
    var navBar: PUPCustomBar? {
        didSet {
            prepareButtonExit()
        }
    }
    
    static var initController: UIViewController? {
        let vc = QQAProfileViewController.initialControllerType()
            
        let scroll = vc?.scrollContainter
        scroll?.handlerViewDidLoad = {
            scroll?.prepareNavigator("Профиль")
            vc?.navBar = scroll?.navBar
        }
        return scroll
    }

    @IBOutlet weak var photoVoew: RNSProfilePhoto!
    @IBOutlet weak var emptyView: QQAProfileEmptyView!
    @IBOutlet weak var fullView: QQAProfileFullView!
    var buttonExit: UIButton?
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    
    var item: QQAUserItem?
    
    var testToken: String = "sdfdsf"
    
    var isHaveToken: Bool {
        return !testToken.isEmpty
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareButtonExit()
        checkToken()
        prepareHandler()
        reloadData()
    }
    
    func reloadData() {
        item = QQAUserItem.stub
        reloadUI()
    }
    
    func prepareHandler() {
        emptyView.handlerAction = { [weak self] in
            self?.showAuth()
        }
    }
    
    func showAuth() {
        PUPAuthManager.authIfNeed { [weak self] in
            self?.testToken = "sdfdsfsdf"
            self?.checkToken()
        }
    }
}
