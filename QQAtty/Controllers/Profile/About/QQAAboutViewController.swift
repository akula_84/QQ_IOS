//
//  QQAAboutViewController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 10.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAAboutViewController: UIViewController {
    
    static var initController: UIViewController? {
        let vc = QQAAboutViewController.initialController
        let scroll = vc?.scrollContainter
        scroll?.handlerViewDidLoad = {
            scroll?.prepareNavigator("О приложении")
            scroll?.prepareBackButton() {
                STRouter.popMain()
            }
        }
        return scroll?.redContainer
    }

    @IBOutlet weak var versionLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        versionLabel.text = Utils.versionBundle
    }
    

}
