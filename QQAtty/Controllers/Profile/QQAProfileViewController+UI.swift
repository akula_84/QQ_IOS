//
//  QQAProfileViewController+UI.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQAProfileViewController {
    
    func prepareButtonExit() {
        guard let navBar = navBar else {
            return
        }
        let button = UIButton(type: .system)
        button.setTitle("Выход", for: UIControlState())
        button.setTitleColor(.white, for: UIControlState())
        button.titleLabel?.font = .sfMedium17
        button.touchUpInside { [weak self] in
            self?.actionExit()
        }
        navBar.addSubview(button)
        button.snp.makeConstraints {
            $0.bottom.right.equalToSuperview()
            $0.width.equalTo(75)
            $0.height.equalTo(45)
        }
        buttonExit = button
    }
    
    func reloadUI() {
        cityLabel.text = item?.сityName
        nameLabel.text = item?.firstName
        lastNameLabel.text = item?.lastName
        phoneLabel.text = InputFieldsValidator.format(item?.phone).text
        mailLabel.text = item?.email
    }
    
    func checkToken() {
        emptyView.isHidden = isHaveToken
        fullView.isHidden = !isHaveToken
        buttonExit?.isHidden = !isHaveToken
    }
}
