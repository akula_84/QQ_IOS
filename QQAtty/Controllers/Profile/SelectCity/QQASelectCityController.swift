//
//  QQASelectCityController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQASelectCityController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var handlerItem: ((QQACityItem?) -> ())?
    
    var items: [QQACityItem]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadItems()
    }
    
    func loadItems() {
        items = [QQACityItem("Москва")
            ,QQACityItem("Санкт-Петербург")
            ,QQACityItem("Абдулино")
            ,QQACityItem("Азов")
            ,QQACityItem("Анапа")
            ,QQACityItem("Ангарск")
            ,QQACityItem("Арзамас")
            ,QQACityItem("Армавир")
            ,QQACityItem("Архангельск")
            ,QQACityItem("Астрахань")
            ,QQACityItem("Балаково"),
             QQACityItem("Балашиха")
            ,QQACityItem("Балашов")
            ,QQACityItem("Барнаул")]
        
        tableView.reloadData()
    }
    


}
