//
//  QQAProfileViewController+Action.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQAProfileViewController {
    
    func actionExit() {
        STRouter.showAlertOkCancel("Выйти из аккаунта", completeOk:{ [weak self] in
            self?.testToken = ""
            self?.checkToken()
        })
    }
    
    @IBAction func actionCity(_ sender: Any) {
        let vc =  QQASelectCityController.initialControllerType()
        vc?.handlerItem = { [weak self] item in
            RNSMenuManager.pop()
            self?.item?.prepareCity(item)
            self?.reloadUI()
        }
        vc?.pushMenu()
    }
    
    @IBAction func actionField(_ sender: Any) {
       guard  let vc = QQABrimViewController.initController({ [weak self] in
            self?.reloadData()
        }, dissmissBack: true) else {
            return
        }
        
       BaseNavigationController(rootViewController: vc).present()
    }
    
    @IBAction func helpAction(_ sender: Any) {
        
    }
    
    @IBAction func aboutAction(_ sender: Any) {
        QQAAboutViewController.initController?.pushMain()
    }
}
