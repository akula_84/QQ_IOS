//
//  QQAPhoneViewController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit
import FRHyperLabel

class QQAPhoneViewController: UIViewController {

    @IBOutlet weak var phoneView: QQAPhoneTextView!
    @IBOutlet weak var buttonCheck: UIButton!
    @IBOutlet weak var labelAgree: FRHyperLabel!
    
    var isCheck = false
    var complete: EmptyBlock?
    var handlerPush: AliasViewController?
    
    static func initController(_ complete: EmptyBlock?) -> UIViewController? {
        let vc = QQAPhoneViewController.initialControllerType()
        vc?.complete = complete
        let scroll = vc?.scrollContainter
        
        scroll?.handlerViewDidLoad = {
            scroll?.prepareNavigator("Авторизация")
            scroll?.prepareBackDissmiss()
        }
        
        guard let red = scroll?.redContainer else {
            return nil
        }
        let nav = BaseNavigationController(rootViewController: red)
        
        vc?.handlerPush = { vc in
            guard let vc = vc else {
                return
            }
            nav.pushViewController(vc, animated: true)
        }
        return nav
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareStub()
        prepareOfertaLink()
    }
    
    func dictAtColor(_ color: UIColor) -> [NSAttributedStringKey: Any] {
        return [NSAttributedStringKey.font: UIFont.system14, NSAttributedStringKey.foregroundColor: color]
    }
    
    func prepareOfertaLink() {
        let text = NSMutableAttributedString()
        let dictDefault = dictAtColor(.color35455A)
        let dictBold = dictAtColor(.color55A8E4)
        let textOffer = "Пользовательским соглашением"
        let textPolit = "Политикой конфеденциальности."
        text.append(NSAttributedString(string: "Я соглашаюсь с ", attributes: dictDefault))
        text.append(NSAttributedString(string: textOffer, attributes: dictBold))
        text.append(NSAttributedString(string: " и ", attributes: dictDefault))
        text.append(NSAttributedString(string: textPolit, attributes: dictBold))
        labelAgree.attributedText = text
        labelAgree.setLinkForSubstring(textOffer) { (_, _) in
            print("оффер нажали")
        }
        
        labelAgree.setLinkForSubstring(textPolit) { (_, _) in
            print("Политику конфиденциальности нажали")
        }
    }
    
    func prepareStub() {
        isCheck = true
        phoneView.text = "+79136298659"
    }
}
