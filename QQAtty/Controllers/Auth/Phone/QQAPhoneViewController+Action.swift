//
//  QQAPhoneViewController+Action.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQAPhoneViewController {
    
    @IBAction func actionCheck(_ sender: Any) {
        buttonCheck.setImage(#imageLiteral(resourceName: "checkActive"), for: UIControlState())
        isCheck = true
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if !phoneView.isValid {
            STRouter.showAlert("Введите корректный номер телефона")
            return
        }
        if !isCheck {
            STRouter.showAlert("Для продолжения работы нужно согласится с Пользовательским соглашением и Политикой конфеденциальности.")
            return
        }
        
        let vc = QQACodeViewController.initController(phoneView.number, complete: complete)
        handlerPush?(vc)
    }
}
