//
//  QQACodeViewController+Action.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQACodeViewController {
    @IBAction func sendCodeAction(_ sender: Any) {
        
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if !textView.isValid {
            STRouter.showAlert("Введите корректный код подтверждения")
            return
        }
        buttonSendComplete()
        handlerPush?(QQABrimViewController.initController(complete))
    }
}
