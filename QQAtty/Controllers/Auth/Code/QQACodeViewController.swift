//
//  QQACodeViewController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQACodeViewController: UIViewController {
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var textView: RNSCodeTextView!
    @IBOutlet weak var buttonSend: UIButton!
    
    var complete: EmptyBlock?
    var phone: String?
    var handlerPush: AliasViewController?
    
    static func initController(_ phone: String?, complete: EmptyBlock?) -> UIViewController? {
        let vc = QQACodeViewController.initialControllerType()
        vc?.complete = complete
        vc?.phone = phone
        let scroll = vc?.scrollContainter
        let red = scroll?.redContainer
        scroll?.handlerViewDidLoad = {
            scroll?.prepareNavigator("Подтверждение")
            scroll?.prepareBackButton(handlerAction: {
                let nav = red?.navigationController
                nav?.popViewController(animated: true)
            })
        }
        
        vc?.handlerPush = { vc in
            let nav = red?.navigationController
            guard let vc = vc else {
                return
            }
            nav?.pushViewController(vc, animated: true)
        }
        return red
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareUI()
        prepareStub()
    }
    
    func prepareStub() {
        textView.text = "111"
    }
    
    func prepareUI() {
        phoneLabel.text = InputFieldsValidator.format(phone).text
    }

    func buttonSendComplete() {
        buttonSend.setTitle("Номер подтвержден", for: UIControlState())
        buttonSend.isEnabled = false
        buttonSend.setTitleColor(.color6FB247, for: UIControlState())
    }
}
