//
//  QQABrimViewController+Action.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQABrimViewController {
    
    @IBAction func nextAction(_ sender: Any) {
        if !nameField.isValid {
            STRouter.showAlert("Введите Имя")
            return
        }
        
        if !lastNameField.isValid {
            STRouter.showAlert("Введите Фамилию")
            return
        }
        
        if !mailField.isValid {
            STRouter.showAlert("Введите корректную электронную почту")
            return
        }
        
        handlerPush?()
    }
}
