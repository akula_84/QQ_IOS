//
//  QQABrimViewController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQABrimViewController: UIViewController {
    
    var complete: EmptyBlock?
    var handlerPush: EmptyBlock?
    
    static func initController(_ complete: EmptyBlock?, dissmissBack: Bool = false) -> UIViewController? {
        let vc = QQABrimViewController.initialControllerType()
        vc?.complete = complete
        let scroll = vc?.scrollContainter
        let red = scroll?.redContainer
        scroll?.handlerViewDidLoad = {
            scroll?.prepareNavigator("Редактирование аккаунта")
            let image: UIImage = dissmissBack ? #imageLiteral(resourceName: "cross") : #imageLiteral(resourceName: "shevronLeft")
            scroll?.prepareBackButton(newIcon: image, handlerAction: {
                let nav = red?.navigationController
                if dissmissBack {
                    nav?.dismiss(animated: true)
                }else {
                    nav?.popViewController(animated: true)
                }
                
            })
        }
        
        vc?.handlerPush = {
            red?.navigationController?.dismiss(animated: true, completion: {
                complete?()
            })
        }
        return red
    }
    
    
    @IBOutlet weak var nameField: RNSSupportTextView!
    @IBOutlet weak var lastNameField: RNSSupportTextView!
    @IBOutlet weak var mailField: RNSSupportTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareStub()
    }
    
    func prepareStub() {
        nameField.text = "Тестовое Имя"
        lastNameField.text = "Тестовая Фамилия"
        mailField.text = "sdfsdf@dsf.rt"
    }
}
