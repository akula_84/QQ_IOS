//
//  QQAMyQuestsController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 03.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAMyQuestsController: UIViewController {

    @IBOutlet weak var questsTable: QQAQuestsTable!
    @IBOutlet weak var emptyView: QQAEmptyItemsView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepareData()
        prepareHandlers()
        updateEmptyView()
    }
    
    func prepareData() {
        STRouter.showLoader()
        Utils.queueUserInitiated {
            let items = QQAStubManager.questsMy
            Utils.mainQueue {
                STRouter.removeLoader()
                self.questsTable.items = items
                self.updateEmptyView()
            }
        }
    }
    
    var isEmptyItems: Bool {
        return questsTable.items?.isEmpty ?? true
    }
    
    func prepareHandlers() {
        emptyView.handlerAction = { [weak self] in
            STRouter.showCreateQuest {
                  self?.prepareData()
            }
        }
    }
    
    func updateEmptyView() {
        emptyView.isHidden = !isEmptyItems
        questsTable.isHidden = isEmptyItems
    }
}
