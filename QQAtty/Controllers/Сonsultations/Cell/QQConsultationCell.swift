//
//  QQСonsultationsCell.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQConsultationCell: RNSBaseTableCell {

    @IBOutlet weak var dateOneLabel: UILabel!
    @IBOutlet weak var dateTwoLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    
    var item: QQAConsultationItem? {
        didSet {
            updateitem()
        }
    }
    
    func updateitem() {
        let time = item?.time
        dateOneLabel.text = time?.stringD_MMMM_yyyy
        dateTwoLabel.text = time?.stringEEEE_HHmm.capitalized
        addressLabel.text = item?.address
        phoneLabel.text = InputFieldsValidator.format(item?.phone).text
    }
}
