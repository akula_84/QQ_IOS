//
//  QQAСonsultationsViewController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAConsultationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var emptyView: QQAEmptyItemsView!
    
    var items: [QQAConsultationItem]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        prepareData()
        prepareHandlers()
        updateEmptyView()
    }
    
    func prepareData() {
        items = QQAStubManager.consultations
        updateEmptyView()
        tableView.reloadData()
    }
    
    var isEmptyItems: Bool {
        return items?.isEmpty ?? true
    }
    
    func prepareHandlers() {
        emptyView.handlerAction = { [weak self] in
            STRouter.showCreateConsultation() {
                self?.prepareData()
            }
        }
    }
    
    func updateEmptyView() {
        emptyView.isHidden = !isEmptyItems
        tableView.isHidden = isEmptyItems
    }
}
