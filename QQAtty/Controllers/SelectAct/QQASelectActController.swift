//
//  QQASelectActController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 03.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQASelectActController: UIViewController {

    var handlerClose: EmptyBlock?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        animateShow()
    }
    
    func animateShow() {
        view.alpha = 0
        UIView.animate(withDuration: 0.5) {
            self.view.alpha = 1
        }
    }
    
    func dismiss(completion: (() -> Swift.Void)? = nil) {
        handlerClose?()
        dismiss(animated: false, completion: completion)
    }
    

}
