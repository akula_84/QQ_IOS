//
//  QQASelectActController+Action.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQASelectActController {
    
    @IBAction func createQuestAction(_ sender: Any) {
        dismiss {
            STRouter.showCreateQuest()
        }
    }
    
    @IBAction func createСonsultation(_ sender: Any) {
        dismiss {
            STRouter.showCreateConsultation()
        }
    }
    
    @IBAction func anyAction(_ sender: Any) {
        dismiss()
    }
    
}
