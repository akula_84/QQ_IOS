//
//  QQACreateQuestController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQACreateQuestController: UIViewController {
    
    var complete: EmptyBlock?
    
    static func initController(_ complete: EmptyBlock? = nil) -> UIViewController? {
        let vc = QQACreateQuestController.initialControllerType()
        vc?.complete = complete
        let scroll = vc?.scrollContainter
        scroll?.handlerViewDidLoad = {
            scroll?.prepareNavigator("Задать вопрос")
            scroll?.prepareBackDissmiss()
        
        }
        return scroll?.redContainer
    }
    
    @IBOutlet weak var titleView: RNSSupportTextView!
    @IBOutlet weak var detailView: RNSSupportTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareStub()
    }
    
    func prepareStub() {
        titleView.text = "Tecт"
        detailView.text = "Tecт"
    }
    
    var titleText: String? {
        return titleView.text
    }
    
    var detailText: String? {
        return detailView.text
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if titleText?.isEmpty ?? true {
            showAlert("Введите вопрос!")
            return
        }
        
        if detailText?.isEmpty ?? true {
            showAlert("Введите текст вопроса!")
            return
        }
        
        PUPAuthManager.authIfNeed { [weak self] in
            print("Логин сделан")
            QQAStubManager.prepareQuestsMy()
            self?.dismiss(animated: true, completion: self?.complete)
        }
    }
    
    func showAlert(_ text: String? = "Ошибка") {
        STRouter.showAlert(text)
    }
}
