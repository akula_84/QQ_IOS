//
//  QQACreateСonsultationController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQACreateConsultationController: UIViewController {
    
    var complete: EmptyBlock?
    
    static func initController(_ complete: EmptyBlock? = nil) -> UIViewController? {
        let vc = QQACreateConsultationController.initialControllerType()
        vc?.complete = complete
        let scroll = vc?.scrollContainter
        scroll?.handlerViewDidLoad = {
            scroll?.prepareNavigator("Записаться")
            scroll?.prepareBackDissmiss()
            
        }
        return scroll?.redContainer
    }
    
    @IBOutlet weak var metroTextView: RNSSupportTextView!
    @IBOutlet weak var timeTextView: RNSSupportTextView!
    
    
    /// Текущая дата
    var currentMetro: QQAMetroItem?
    var currentDate: Date?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    var isHaveDate: Bool {
        return currentDate != nil
    }
    
    func prepareDate(_ date: Date?) {
        currentDate = date
        updateDate()
    }
    
    func prepareMetro(_ item: QQAMetroItem?) {
        currentMetro = item
        metroTextView.text = item?.name
    }
    
    func updateDate() {
        timeTextView.text = isHaveDate ? currentDate?.stringE_d_MMMM : ""
    }
    
    func showAlert(_ text: String? = "Ошибка") {
        STRouter.showAlert(text)
    }
    
}
