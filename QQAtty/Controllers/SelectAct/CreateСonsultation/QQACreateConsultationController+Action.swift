//
//  QQACreateConsultationController+Action.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQACreateConsultationController {
    
    @IBAction func actionMetro(_ sender: Any) {
        let vc = QQAMetroSelectController.initialControllerType()
        vc?.handlerItem = { [weak self] item in
            self?.prepareMetro(item)
        }
        vc?.present()
    }
    
    @IBAction func actionTime(_ sender: Any) {
        let vc = RNSDateSelectViewController.initialControllerType()
        vc?.startDate = currentDate
        vc?.transitioningDelegate = CustomTransitionDelegate.shared
        vc?.handlerDate = { [weak self] date in
            self?.prepareDate(date)
        }
        STRouter.present(vc)
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if currentMetro == nil {
            showAlert("Введите метро!")
            return
        }
        
        if currentDate == nil {
            showAlert("Введите дату")
            return
        }
        
        PUPAuthManager.authIfNeed { [weak self] in
            print("Логин сделан")
            QQAStubManager.prepareConsultations()
            self?.dismiss(animated: true, completion: self?.complete)
        }
    }
}
