//
//  QQAMetroSelectController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAMetroSelectController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var handlerItem: ((QQAMetroItem?) -> ())?
    
    var items: [QQAMetroItem]?

    override func viewDidLoad() {
        super.viewDidLoad()

        loadItems()
    }
    
    func loadItems() {
        items = [QQAMetroItem("Александровский сад")
                ,QQAMetroItem("Авиамоторная")
         ,QQAMetroItem("Алексеевская")
         ,QQAMetroItem("Алма-Атинская")
         ,QQAMetroItem("Альтуфьево")
         ,QQAMetroItem("Аннино")
         ,QQAMetroItem("Арбатская")
         ,QQAMetroItem("Аэропорт")
         ,QQAMetroItem("Бабушкинская")
         ,QQAMetroItem("Багратиовская")
         ,QQAMetroItem("Бауманская")]
        
        tableView.reloadData()
    }


}
