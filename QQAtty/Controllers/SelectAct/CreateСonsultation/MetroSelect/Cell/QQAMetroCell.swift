//
//  QQAMetroCell.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAMetroCell: RNSBaseTableCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var galkaImage: UIImageView!
    
    var title: String? {
        didSet {
            titleLabel.text = title 
        }
    }
    
    func prepareSelect() {
        titleLabel.textColor = .color5796DE
        galkaImage.isHidden = false
    }

}
