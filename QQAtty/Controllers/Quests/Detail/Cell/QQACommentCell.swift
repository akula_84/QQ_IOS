//
//  QQACommentCell.swift
//  QQAtty
//
//  Created by Артем Кулагин on 03.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQACommentCell: RNSBaseTableCell {
    
    @IBOutlet weak var userView: QQAUserView!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var buttomView: QQAGradientView!
    
    var handlerUpdateItem: EmptyBlock?
    
    var item: QQAQuestItem? {
        didSet {
            updateItem()
        }
    }
    
    func updateItem() {
        userView.item = item
        descLabel.attributedText = item?.descriptionAttr
        buttomView.isHidden = !(item?.isShowOpenButtonComments ?? true)
    }
    
    func updateButtonOpen() {
        let title = (item?.openDetail ?? false) ? "Свернуть" : "Развернуть"
        openButton.setTitle(title, for: UIControlState())
        
    }
    
    func updateHeight() {
        updateButtonOpen()
        handlerUpdateItem?()
    }
    
    @IBAction func openAction(_ sender: Any) {
        item?.revertOpen()
        updateHeight()
    }
}
