//
//  QQAQuestDetailController+UI.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQAQuestDetailController {
    
    func prepareUI() {
        
        prepareTableView()
        prepareHeader()
    }
    
    func updateHeader() {
        headerView.item = item
    }
    
    func prepareTableView() {
        if #available(iOS 11.0, *) {
        } else {
            automaticallyAdjustsScrollViewInsets = false;
            tableView.contentInset = UIEdgeInsetsMake(0,0,0,0);
        }
    }
    
    func prepareHeader() {
        tableView.tableHeaderView = headerView
        //updateTableViewHeaderViewHeight()
    }
    
    func updateTableViewHeaderViewHeight() {
        tableView.updateTableViewHeaderViewHeight()
    }

}
