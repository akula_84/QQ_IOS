//
//  QQAQuestDetailController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAQuestDetailController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    lazy var headerView: QQADetailHeaderView = {
        let view = QQADetailHeaderView()
        view.handlerUpdateItem = {
            self.updateTableViewHeaderViewHeight()
        }
        return view
    }()
    
    var item: QQAQuestItem?
    var items: [QQAQuestItem]?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareUI()
        loadData()
    }
}
