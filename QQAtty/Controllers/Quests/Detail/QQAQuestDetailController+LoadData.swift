//
//  QQAQuestDetailController+LoadData.swift
//  QQAtty
//
//  Created by Артем Кулагин on 03.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQAQuestDetailController {
    
    func loadData() {
        loadHeader()
        loadComments()
    }
    
    func loadHeader() {
        STRouter.showLoader()
        Utils.queueUserInitiated {
            self.item = QQAQuestItem.detailItem
            Utils.mainQueue {
                self.updateHeader()
                STRouter.removeLoader()
            }
        }
    }
    
    func loadComments() {
        STRouter.showLoader()
        Utils.queueUserInitiated {
            self.items = QQAQuestItem.detailItemComments
            Utils.mainQueue {
                self.reloadTableView()
                STRouter.removeLoader()
            }
        }
    }
}
