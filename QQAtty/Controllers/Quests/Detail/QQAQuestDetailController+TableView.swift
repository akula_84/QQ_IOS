//
//  QQAQuestDetailController+TableView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 03.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQAQuestDetailController: UITableViewDelegate, UITableViewDataSource {
    
    func reloadTableView() {
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return item(indexPath)?.heightComments ?? 250
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath) as QQACommentCell
        cell.item = item(indexPath)
        cell.handlerUpdateItem = { [weak self] in
            self?.reloadTableView()
        }
        return cell
    }
    
    func item(_ indexPath: IndexPath) -> QQAQuestItem? {
        return items?[indexPath.row]
    }
}
