//
//  QQAQuestsViewController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAQuestsViewController: UIViewController, KeyboardShowable {

    var isFavorite: Bool {
        return bar.isFavorite
    }
    
    @IBOutlet weak var questsTable: QQADefaultQuestsTable!
    @IBOutlet weak var favoritesTable: QQAFavoritesTable!
    
    @IBOutlet weak var bar: QQAQuestBarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateFavor()
        prepareHandlers()
    }
    
    func prepareHandlers() {
        bar.handlerFavor = { [weak self] in
            self?.updateFavor()
        }
    }
    
    func updateFavor() {
        questsTable.isHidden = isFavorite
        favoritesTable.isHidden = !isFavorite
        
        if isFavorite {
            favoritesTable.loadData()
        } else {
            questsTable.loadData()
        }
    }

    /// метод обработки появления контроллера
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addKeyboardObservers()
    }
    
    /// метод обработки исчезновения контроллера
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeAllObservers()
    }

}
