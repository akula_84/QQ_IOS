//
//  QQATapBarViewController.swift
//  QQAtty
//
//  Created by Артем Кулагин on 01.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQMenuViewController: UIViewController {
    @IBOutlet var containerView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareHandlers()
        updateContainer()
    }
    
    func prepareHandlers() {
        RNSMenuManager.handlerUpdateContainer = { [weak self] in
            self?.updateContainer()
        }
    }
    
    func updateContainer() {
        removeOldContainer()
        addCurrent()
    }
    
    func addCurrent() {
        guard let containerVC = RNSMenuManager.currentItem?.vc,
            let view = containerVC.view else {
                return
        }
        self.addChildViewController(containerVC)
        containerView.addSubview(view)
        view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func removeOldContainer() {
        childViewControllers.forEach {
            $0.view.removeFromSuperview()
            $0.removeFromParentViewController()
        }
    }
}
