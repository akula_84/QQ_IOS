//
//  STRouter+Alert.swift
//  PickUpp
//
//  Created by Артем Кулагин on 05.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import Foundation
import UIKit

extension STRouter {
    
    static func showAlert(_ text: String? = "Ошибка", title: String? = "Сообщение", buttonTitle: String? = "OK", complete: EmptyBlock? = nil) {
        let alert = self.alert(text, title: title, buttonTitle: buttonTitle, complete: complete)
        present(alert)
    }
    
    static func alert(_ text: String? = "Ошибка", title: String? = "Сообщение", buttonTitle: String? = "OK", complete: EmptyBlock? = nil) -> UIAlertController {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: buttonTitle, style: .cancel, handler: { _ in
            complete?()
        })
        alert.addAction(cancelAction)
        return alert
    }
    
    static func showAttention(_ text: String? = "Ошибка", complete: EmptyBlock? = nil) {
        showAlert(text, title: "Внимание", complete: complete)
    }
    
    static func showAlertOkCancel(_ text: String? = "Ошибка", title: String? = "Сообщение", titleOk: String? = "OK", completeOk: EmptyBlock? = nil, titleCancel: String? = "Отмена", completeCancel: EmptyBlock? = nil) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addActionWith(title: titleOk, style: .default, handler: { item in
            completeOk?()
        })
        alert.addActionWith(title: titleCancel, style: .cancel, handler: { item in
            completeCancel?()
        })
        present(alert)
    }
}
