//
//  STRouter.swift
//  Spytricks
//
//  Created by Артем Кулагин on 10.02.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import UIKit
/**
 Менеджер отображения контроллеров
 */
class STRouter: NSObject {
    
    /// переменная синглетона
    static let shared = STRouter()

}
