//
//  STRouter+Navigation.swift
//  RNIS
//
//  Created by Артем Кулагин on 21.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

/**
 Расширение для работы с навигацией
 */
extension STRouter {
    static func showCreateQuest(_ complete: EmptyBlock? = nil) {
        QQACreateQuestController.initController(complete)?.present()
    }
    
    static func showCreateConsultation(_ complete: EmptyBlock? = nil) {
        QQACreateConsultationController.initController(complete)?.present()
    }
}
