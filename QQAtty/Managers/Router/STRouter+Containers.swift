//
//  STRouter+Containers.swift
//  RNIS
//
//  Created by Артем Кулагин on 31.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

/**
 Расширение для работы с контейнер
 */
extension STRouter {
    
    static func imageContainer(_ viewController: UIViewController? = nil) -> UIViewController? {
        return redContainer(viewController)
    }
    
    static func redContainer(_ viewController: UIViewController?) -> UIViewController? {
        return RNSRedContainer(viewController)
    }
    
    static func scrollContainer(_ viewController: UIViewController?, isNeedAddTap: Bool = true) -> RNSScrollKeyBoardContainer? {
        return RNSScrollKeyBoardContainer.initController(viewController, isNeedAddTap: isNeedAddTap) 
    }
    
    static func imageScrollContainer(_ viewController: UIViewController?) -> UIViewController? {
        return imageContainer(scrollContainer(viewController))
    }

    static func redScrollContainer(_ viewController: UIViewController?) -> UIViewController? {
        return redContainer(scrollContainer(viewController))
    }
}
