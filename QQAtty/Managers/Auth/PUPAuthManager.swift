//
//  RNISAuthManager.swift
//  RNIS
//
//  Created by Артем Кулагин on 21.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

/**
 Контроллер авторизации
 */
class PUPAuthManager {
    
    static func authIfNeed(complete: EmptyBlock?) {
        if isHaveToken {
            complete?()
            return
        }
        QQAPhoneViewController.initController(complete)?.present()
    }

    
    static var isHaveToken: Bool {
        return false
    }
    /*
    static func loginLaunch() {
        if !UserDefaults.isHaveToken {
            showLogin()
            return
        }
        STRouter.showMain()
        /*
        loginCheckProfile(false) { error in
            showSign()
        }
        */
    }
    
    static func logout() {
        STRouter.logout()
        showLogin()
        RNSMenuManager.showFirst()
    }
    
    static func showLogin() {
        STRouter.showLogin()
    }
  */
}
