//
//  QQAStubManager.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAStubManager {
    static var questsMy: [QQAQuestItem]?
    
    static func prepareQuestsMy() {
        questsMy = QQAQuestItem.stubsMy
    }
    
    static var consultations: [QQAConsultationItem]?
    
    static func prepareConsultations() {
        consultations = QQAConsultationItem.stubs
    }
    
    static var favorites: [QQAQuestItem]?
    
    static func prepareFavorites() {
        favorites = QQAQuestItem.stubsFavorite
    }
}
