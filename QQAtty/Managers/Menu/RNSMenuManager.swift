//
//  RNSMenuManager.swift
//  RNIS
//
//  Created by Артем Кулагин on 01.09.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit
/**
 Контоллер управления боковым меню
 */
class RNSMenuManager: NSObject {
    
    /// Создание экземпляра менеджера меню
    static let shared = RNSMenuManager()
    
    /// Блок выбранного контроллера меню
    static var handlerUpdateItems: EmptyBlock?
    static var handlerUpdateContainer: EmptyBlock?
    
    /// Создание экземпляра раздела меню
    static var menuItems: [MenuItem] {
        return shared.menuItems
    }
    
    static var currentItem: MenuItem? = menuItems.first
    
    /// Создание пунктов меню
    lazy var menuItems = [MenuItem(#imageLiteral(resourceName: "QuestActive"),#imageLiteral(resourceName: "QuestPassive"), QQAQuestsViewController.initialController)
                         ,MenuItem(#imageLiteral(resourceName: "mainQuestActive"),#imageLiteral(resourceName: "mainQuestPassive"), QQAMyQuestsController.initialController)
                         ,MenuItem(#imageLiteral(resourceName: "exclamationActive"),#imageLiteral(resourceName: "exclamationPassive"), QQAConsultationsViewController.initialController)
                         ,MenuItem(#imageLiteral(resourceName: "profileActive"),#imageLiteral(resourceName: "profilePassive"), QQAProfileViewController.initController)]
    

    
    
    /// Функция отображения выбранного контроллера меню
    static func showItem(_ item: MenuItem?) {
        if currentItem == item {
            return
        }
        currentItem = item
        handlerUpdateItems?()
        handlerUpdateContainer?()
    }
    
    static func showItem(_ index: Int) {
        showItem(menuItems.valueAt(index))
    }
    
    static func showFirst() {
        showItem(menuItems.first)
    }

}
