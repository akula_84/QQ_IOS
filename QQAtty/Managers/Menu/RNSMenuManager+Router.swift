//
//  RNSMenuManager+Router.swift
//  PickUpp
//
//  Created by Артем Кулагин on 12.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import UIKit

extension RNSMenuManager {
    /// Переменная содержащая корневой контроллер
    static var rootViewController: BaseNavigationController? {
        return currentItem?.vc
    }
    /// Переменная содержащая корневое вью
    static var rootView: UIView? {
        return rootViewController?.view
    }
    
    static var viewControllers: [UIViewController]? {
        return rootViewController?.viewControllers
    }
    
    static func push(_ viewController: UIViewController?,
                     animated: Bool = false,
                     completion: EmptyBlock? = nil) {
        guard let viewController = viewController else {
            return
        }
        rootViewController?.push(viewController, animated: animated, completion: completion)
    }
    
    static func pushAnimated(_ viewController: UIViewController?, animated: Bool = true, completion: EmptyBlock? = nil) {
        push(viewController, animated: animated, completion: completion)
    }
    
    static func pop(animated: Bool = true, completion: EmptyBlock? = nil) {
        rootViewController?.pop(animated: animated, completion: completion)
    }

    
    static func popToRoot(_ animated: Bool = true, completion:EmptyBlock? = nil) {
        rootViewController?.popToRoot(animated: animated, completion: completion)
    }
    
    static func popNoAnimate(completion: EmptyBlock? = nil) {
        pop(animated: false, completion: completion)
    }
    
    static func clearNav() {
        rootViewController?.viewControllers.removeAll()
    }
}
