//
//  UIFont+AppDefaultFonts.swift
//  Spytricks
//
//  Created by Ivan Alekseev on 13.09.16.
//  Copyright © 2016 Ivan Alekseev. All rights reserved.
//

import UIKit

/**
 Расширение для UIFont
 */
extension UIFont {
    
    
    static var system12: UIFont {
        return system(12)
    }
    
    static var system13: UIFont {
        return system(13)
    }
    
    static var system14: UIFont {
        return system(14)
    }
    
    static var system15: UIFont {
        return system(15)
    }
    
    static var systemBold15:UIFont {
        return .boldSystemFont(ofSize: 15)
    }
    
    static var systemMedium16: UIFont {
        return systemMedium(16)
    }
    
    static var systemMedium17: UIFont {
        return systemMedium(17)
    }
    
    static func systemMedium(_ size: CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Medium", size: size) ?? UIFont()
    }
    
    static func system(_ size: CGFloat) -> UIFont {
        return .systemFont(ofSize: size)
    }
    
    static var sfRegular15: UIFont {
        return sfRegular(15)
    }
    
    static var sfMedium17: UIFont {
        return sfMedium(17)
    }
    
    static func sfRegular(_ size: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-Regular", size: size)!
    }
    
    static func sfMedium(_ size: CGFloat) -> UIFont {
        return UIFont(name: "SFProDisplay-Medium", size: size)!
    }
}
