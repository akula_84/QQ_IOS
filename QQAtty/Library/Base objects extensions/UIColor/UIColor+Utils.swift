//
//  UIColor+Utils.swift
//  RNIS
//
//  Created by Артем Кулагин on 25.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import Foundation
import UIKit

/**
 Расширение для UIColor
 */
extension UIColor {

    static var DCDCDC: UIColor {
        return #colorLiteral(red: 0.862745098, green: 0.862745098, blue: 0.862745098, alpha: 1)
    }
    
    static var color77ABE6: UIColor {
        return #colorLiteral(red: 0.4666666667, green: 0.6705882353, blue: 0.9019607843, alpha: 1)
    }
    
    static var color278FE5: UIColor {
        return #colorLiteral(red: 0.1529411765, green: 0.5607843137, blue: 0.8980392157, alpha: 1)
    }
    
    static var color8E9DB0: UIColor {
        return #colorLiteral(red: 0.5568627451, green: 0.6156862745, blue: 0.6901960784, alpha: 1)
    }
    
    static var color55A8E4: UIColor {
        return #colorLiteral(red: 0.3333333333, green: 0.6588235294, blue: 0.8941176471, alpha: 1)
    }
    
    static var color5796DE: UIColor {
        return #colorLiteral(red: 0.3411764706, green: 0.5882352941, blue: 0.8705882353, alpha: 1)
    }
    
    static var color81B2E9: UIColor {
        return #colorLiteral(red: 0.5058823529, green: 0.6980392157, blue: 0.9137254902, alpha: 1)
    }
    
    
    static var colorF4F4F6: UIColor {
        return #colorLiteral(red: 0.9568627451, green: 0.9568627451, blue: 0.9647058824, alpha: 1)
    }
    
    static var color72839D: UIColor {
        return #colorLiteral(red: 0.4470588235, green: 0.5137254902, blue: 0.6156862745, alpha: 1)
    }
    
    static var colorA7ACB6: UIColor {
        return #colorLiteral(red: 0.6549019608, green: 0.6745098039, blue: 0.7137254902, alpha: 1)
    }
    
    static var colorF7F7FA: UIColor {
        return #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.9803921569, alpha: 1)
    }
    
    static var color494949: UIColor {
        return #colorLiteral(red: 0.2862745098, green: 0.2862745098, blue: 0.2862745098, alpha: 1)
    }
    
    static var color35455A: UIColor {
        return #colorLiteral(red: 0.2078431373, green: 0.2705882353, blue: 0.3529411765, alpha: 1)
    }
    
    static var color6FB247: UIColor {
        return #colorLiteral(red: 0.4352941176, green: 0.6980392157, blue: 0.2784313725, alpha: 1)
    }
    
    static var color5695DD: UIColor {
        return #colorLiteral(red: 0.337254902, green: 0.5843137255, blue: 0.8666666667, alpha: 1)
    }
    
    static var colorFDFDFD: UIColor {
        return #colorLiteral(red: 0.9921568627, green: 0.9921568627, blue: 0.9921568627, alpha: 1)
    }
    
    static var color78849E: UIColor {
        return #colorLiteral(red: 0.4705882353, green: 0.5176470588, blue: 0.6196078431, alpha: 1)
    }
}
