//
//  UIView+InnerShadow.swift
//  Spytricks
//
//  Created by Ivan Alekseev on 08.09.16.
//  Copyright © 2016 Ivan Alekseev. All rights reserved.
//

import UIKit

private var xoAssociationKey: UInt8 = 0
extension UIView {
    
    var innerShadow: CALayer? {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? CALayer
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
    
    func addInnerShadowWithRadius(_ radius: CGFloat, andAlpha: CGFloat) {
        if self.innerShadow == nil  {
            let innerShadow = createInnerShadow(radius: radius, color: UIColor(white: 0, alpha: andAlpha))
            layer.addSublayer(innerShadow)
            self.innerShadow = innerShadow
        }
        self.innerShadow?.frame = bounds
    }
    
    func addInnerShadowWithRadius(_ radius: CGFloat, color: UIColor) {
        if self.innerShadow == nil  {
            let innerShadow = createInnerShadow(radius: radius, color: color)
            layer.addSublayer(innerShadow)
            self.innerShadow = innerShadow
        }
        self.innerShadow?.frame = bounds
    }
    
    func createInnerShadow(radius: CGFloat, color: UIColor) -> CALayer {
        let innerShadow = CALayer()
        innerShadow.frame = bounds
        
        let path = UIBezierPath(roundedRect: bounds.insetBy(dx: -1*radius, dy: -1*radius), cornerRadius: self.layer.cornerRadius)
        let cutout = UIBezierPath(roundedRect: bounds, cornerRadius: self.layer.cornerRadius).reversing()
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        
        innerShadow.shadowColor = color.cgColor
        innerShadow.shadowOffset = .zero
        innerShadow.shadowOpacity = 1
        innerShadow.shadowRadius = radius
        return innerShadow;
    }
}
