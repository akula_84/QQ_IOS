//
//  UIView+Utils.swift
//  RNIS
//
//  Created by Артем Кулагин on 21.07.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit
//import SnapKit

extension UIView {
    
    func addBackgroundImage() {
        let view = UIImageView(image: #imageLiteral(resourceName: "mainBackground"))
        view.contentMode = .redraw
        addSubview(view)
        
        view.snp.makeConstraints {
            $0.top.bottom.equalTo(self).offset(0)
            $0.left.right.equalTo(self).inset(0)
        }
    }
    
    static func animateConstrains(_ target: UIView?, withDuration duration: TimeInterval = 0.3, animations: @escaping () -> Void, completion: EmptyBlock? = nil) {
        guard let target = target else {
            animations()
            return
        }
        target.layoutIfNeeded()
        
        UIView.animate(withDuration: duration, animations: {
            animations()
            target.layoutIfNeeded()
        }) { value in
            completion?()
        }
    }
    
    func animateConstrains(withDuration duration: TimeInterval = 0.3, animations: @escaping () -> Void, completion: EmptyBlock? = nil) {
        layoutIfNeeded()
        
        UIView.animate(withDuration: duration, animations: {
            animations()
            self.layoutIfNeeded()
        }) { value in
            completion?()
        }
    }
    
    
    func animateConstrains(_ duration: TimeInterval = 0.3, animations: @escaping () -> Void, completion: EmptyBlock? = nil) {
        layoutIfNeeded()
        UIView.animate(withDuration: duration, animations: {
            animations()
            self.layoutIfNeeded()
        }) { value in
            completion?()
        }
    }
    
    func removeAllGesture() {
        if let recognizers = self.gestureRecognizers {
            for recognizer in recognizers {
                self.removeGestureRecognizer(recognizer)
            }
        }
    }
    
    func prepareCircle() {
        layer.cornerRadius =  frame.width/2
        layer.masksToBounds = true
    }
    
    func addSubviewEdgeConstraints(_ view: UIView?) {
        guard let view = view else {
            return
        }
        addSubview(view)
        view.snp.makeConstraints{
            $0.edges.equalToSuperview()
        }
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
