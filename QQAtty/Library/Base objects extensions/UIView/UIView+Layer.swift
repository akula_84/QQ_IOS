//
//  UIView+Layer.swift
//  Spytricks
//
//  Created by Артем Кулагин on 16.05.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import UIKit

/**
 Расширение для UIView для Layer
 */
extension UIView {
    /// Переменная содержащая вещественный тип с радиусом угла
    @IBInspectable var cornerRadius: CGFloat {
        get { return 0 }
        set {
            self.layer.cornerRadius = newValue
        }
    }
    /// Переменная содержащая вещественный тип с шириной края
    @IBInspectable var borderWidth: CGFloat {
        get { return 0 }
        set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get { return .clear }
        set {
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var prepareShadow: Bool {
        get { return false }
        set {
            if newValue {
                prepareShadowDefault()
            }
        }
    }
    
    func prepareShadowDefault() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 3
        layer.shadowOffset = CGSize.init(width: 2, height: 2)
    }
    
    func prepareShadowRound() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 5
    }
    
    
}
