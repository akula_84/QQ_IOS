//
//  UIViewController+Utils.swift
//  PickUpp
//
//  Created by Артем Кулагин on 13.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import UIKit

extension UIViewController {
    func prepareContainer(_ containerVC: UIViewController?) {
        guard let containerVC = containerVC,
            let containerView = containerVC.view else {
                return
        }
        self.addChildViewController(containerVC)
        view.addSubview(containerView)
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}
