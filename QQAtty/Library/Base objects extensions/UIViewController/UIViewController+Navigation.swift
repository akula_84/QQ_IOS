//
//  UIViewController+Navigation.swift
//  RNIS
//
//  Created by Артем Кулагин on 04.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

extension UIViewController {
    
    var imageScrollContainter: UIViewController? {
        return scrollContainter?.imageContainter
    }
    
    var scrollContainter: RNSScrollKeyBoardContainer? {
        return STRouter.scrollContainer(self)
    }
    
    var imageContainter: UIViewController? {
        return STRouter.imageContainer(self)
    }
    
    var redContainer: UIViewController? {
        return STRouter.redContainer(self)
    }
  
    static func initialPresent() {
        initialController?.present()
    }
    
    static func initialPushMain() {
        initialController?.pushMain()
    }
    
    func imageScrollPushMain(_ animated: Bool = true, completion: EmptyBlock? = nil) {
        imageScrollContainter?.pushMain(animated, completion: completion)
    }
    
    func pushMain(_ animated: Bool = true, completion: EmptyBlock? = nil) {
        STRouter.pushMain(self, animated: animated, completion: completion)
    }
    
    func present(_ animated: Bool = true) {
        STRouter.present(self, animated: animated)
    }
}
