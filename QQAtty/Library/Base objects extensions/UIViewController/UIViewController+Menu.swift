//
//  UIViewController+Menu.swift
//  PickUpp
//
//  Created by Артем Кулагин on 20.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import UIKit

extension UIViewController {
    func pushMenu(_ animated: Bool = true, completion: EmptyBlock? = nil) {
        RNSMenuManager.push(self, animated: animated, completion: completion)
    }
}
