//
//  NSAttributedString+Utils.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension NSAttributedString {
    
    func height(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = boundingRect(with: constraintRect,
                                            options: [.usesLineFragmentOrigin],
                                            context: nil)
        return ceil(boundingBox.height)
    }
}
