//
//  DateFormatter+Utils.swift
//  Spytricks
//
//  Created by Артем Кулагин on 17.03.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

/**
 Расширение для DateFormatter
 */
extension DateFormatter {
    /// Переменная содержащая строку формата
    static let iso8601: DateFormatter = {
        let formatter = DateFormatter()
        formatter.calendar = Calendar(identifier: .iso8601)
        formatter.locale = Locale(identifier: "en_US_POSIX")
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        return formatter
    }()
    
    static var textddMMyyyy: String {
        return "dd.MM.yyyy"
    }
    
    static var textddMMyy: String {
        return "dd.MM.yy"
    }
    
    static var textHHmm: String {
        return "HH:mm"
    }
    
    static var ddMMyy: DateFormatter{
        return format(textddMMyy)
    }
   
    static var ddMMyyyy: DateFormatter{
        return format(textddMMyyyy)
    }
    
    static var HHmm: DateFormatter{
        return format(textHHmm)
    }
    
    static var dd_LLLL: DateFormatter{
        return format("dd LLLL")
    }
    
    static var E_d_MMMM: DateFormatter{
        return format("E d MMMM" + " / " + textHHmm)
    }
    
    static var EEEE_HHmm: DateFormatter{
        return format("EEEE " + textHHmm)
    }
    
    static var d_MMMM_yyyy: DateFormatter{
        return format("d MMMM yyyy")
    }
    
    static func format(_ format: String) -> DateFormatter {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "ru")
        formatter.dateFormat = format
        return formatter
    }
}
