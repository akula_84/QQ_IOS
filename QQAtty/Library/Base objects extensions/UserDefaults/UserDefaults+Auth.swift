//
//  UserDefaults+Auth.swift
//  Spytricks
//
//  Created by Артем Кулагин on 13.04.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

/**
 Расширение для UserDefaults
 */
extension UserDefaults {
  
    /// Переменная содержащая булево значение есть токен
    static var isHaveToken: Bool {
        return token != nil
    }
    
    static var token: String? {
        get {
            return standard[#function]
        }
        set {
            standard[#function] = newValue
        }
    }
    
    static var login: String? {
        get {
            return standard[#function]
        }
        set {
            standard[#function] = newValue
        }
    }
    
    static var password: String? {
        get {
            return standard[#function]
        }
        set {
            standard[#function] = newValue
        }
    }
}
