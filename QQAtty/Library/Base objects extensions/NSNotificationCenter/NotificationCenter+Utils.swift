//
//  NSNotificationCenter+Utils.swift
//  Solity-iOS
//
//  Created by Artem Kulagin on 23.09.16.
//  Copyright © 2016 el-machine. All rights reserved.
//

import Foundation

extension NotificationCenter {
    static var kUpdateTime = "updateTime"
    static var kUpdateUser = "updateUser"
    static var kUpdateSkimp = "updateSkimp"
    

    static var center: NotificationCenter {
        return NotificationCenter.default
    }
    
    static func post(_ name: String, userInfo: AliasHashable? = nil) {
        center.post(name: Notification.Name(rawValue: name), object: nil, userInfo: userInfo)
    }
    
    static func removeObserver(_ observer: Any) {
        center.removeObserver(observer)
    }
    
    static func addObserver(_ name: String, observer: Any, selector aSelector: Selector) {
        let name = NSNotification.Name(rawValue: name)
        center.removeObserver(observer, name: name, object: nil)
        center.addObserver(observer, selector:aSelector, name: name, object: nil)
    }
    
    static func addObserverDidBecomeActive(_ observer: Any, selector aSelector: Selector) {
        addObserver(Notification.Name.UIApplicationDidBecomeActive.rawValue, observer: observer, selector: aSelector)
    }
    
    static func addObserverTime(_ observer: Any, selector aSelector: Selector) {
        addObserver(kUpdateTime, observer: observer, selector: aSelector)
    }
    
    static func addUpdateUser(_ observer: Any, selector aSelector: Selector) {
        addObserver(kUpdateUser, observer: observer, selector: aSelector)
    }
    
    static func addUpdateSkimp(_ observer: Any, selector aSelector: Selector) {
        addObserver(kUpdateSkimp, observer: observer, selector: aSelector)
    }
    
    static func postTime() {
        post(kUpdateTime)
    }
    
    static func postUpdateUser() {
        post(kUpdateUser)
    }
    
    static func postUpdateSkimp() {
        post(kUpdateSkimp)
    }
    
    
}
