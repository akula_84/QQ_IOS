//
//  Date+Utils.swift
//  RNIS
//
//  Created by Артем Кулагин on 22.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import Foundation

/**
 Расширение для Date
 */
extension Date {
    
    var iso8601: String {
        return DateFormatter.iso8601.string(from: self)
    }
    
    var stringDdMMyyyy: String {
        return DateFormatter.ddMMyyyy.string(from: self)
    }
    
    var stringDdMMyy: String {
        return DateFormatter.ddMMyy.string(from: self)
    }
   
    var stringHHmm: String {
        return DateFormatter.HHmm.string(from: self)
    }
    
    var stringDd_LLLL: String {
        return DateFormatter.dd_LLLL.string(from: self)
    }
    
    /// Переменная содержащая строку формата
    var stringE_d_MMMM: String {
        return DateFormatter.E_d_MMMM.string(from: self)
    }
    
    var stringD_MMMM_yyyy: String {
        return DateFormatter.d_MMMM_yyyy.string(from: self)
    }
    
    var stringEEEE_HHmm: String {
        return DateFormatter.EEEE_HHmm.string(from: self)
    }
    
    var timeEndString: String? {
        if  self.compare(Date(timeIntervalSince1970: 0)) != .orderedSame {
            var  diff = self.timeIntervalSinceNow
            if diff < 0 {
                diff = 0
            }
            let s = Int(diff) % 60
            return "\(s)"
            //return Utils.secondsToStr(seconds: Int(diff))
        }
        return nil
    }
}
