//
//  Date+Calendar.swift
//  PickUpp
//
//  Created by Артем Кулагин on 26.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import Foundation

extension Date {
    
    static var timeStart: Date  {
        return Date().appendMinute(10) ?? Date()
    }
    
    var calendar: Calendar {
        return Calendar.current
    }
    
    func appendMinute(_ value: Int?) -> Date? {
        return dateByAddingUnit(.minute, value: value ?? 0)
    }
    
    func appendSecond(_ value: Int?) -> Date? {
        return dateByAddingUnit(.second, value: value ?? 0)
    }
    
    func appendDay(_ value: Int?) -> Date? {
        return dateByAddingUnit(.day, value: value ?? 0)
    }
    
    func appendHour(_ value: Int?) -> Date? {
        return dateByAddingUnit(.hour, value: value ?? 0)
    }
    
    func dateByAddingUnit(_ unit: Calendar.Component, value: Int) -> Date? {
        return calendar.date(byAdding: unit, value: value, to: self)
    }
    
    var isToday: Bool {
        return calendar.isDateInToday(self)
    }
    
    var isTomorow: Bool {
        return calendar.isDateInTomorrow(self)
    }
    
    var inYesterday: Bool {
        return calendar.isDateInYesterday(self)
    }
    
    var hour: Int {
        return calendar.component(.hour, from: self)
    }
    
    var hourBack: Int {
        let hourCurrent = Date().hour
        let hour = self.hour
        return hourCurrent - hour
    }
    
    var hourBackText: String {
        let hourBack = self.hourBack
        var text = ""
        switch hourBack {
        case 0:
            text = "Только что"
            break
        case 1:
            text = "1 час назад"
            break
        case 2,3,4:
            text = "\(hourBack) часа назад"
            break
        default:
            text = "\(hourBack) часов назад"
            break
        }
        return text
    }
    
    
    func minutes(_ date: Date?) -> Int {
        guard let date = date else {
            return 0
        }
        return calendar.dateComponents([.minute], from: date, to: self).minute ?? 0
    }
}
