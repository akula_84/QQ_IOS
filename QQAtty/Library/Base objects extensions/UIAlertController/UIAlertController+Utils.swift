//
//  UIAlertController+Utils.swift
//  PickUpp
//
//  Created by Артем Кулагин on 07.06.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import UIKit

extension UIAlertController {
    
    func addActionWith(title: String?, style: UIAlertActionStyle = .default, handler: ((UIAlertAction) -> Swift.Void)? = nil) {
        addAction(UIAlertAction(title: title, style: style, handler: handler))
    }
    
    func addActionCancel() {
        addActionWith(title: "Отмена", style: .cancel)
    }
}
