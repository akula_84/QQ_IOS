//
//  Data+Utils.swift
//  Spytricks
//
//  Created by Артем Кулагин on 26.05.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation

extension Data {
    
    
    func jsonObject(options opt: JSONSerialization.ReadingOptions = []) -> AliasDictionary? {
        do {
            return try JSONSerialization.jsonObject(with: self, options: .mutableContainers) as? AliasDictionary
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}
