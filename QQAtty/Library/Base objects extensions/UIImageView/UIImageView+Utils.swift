//
//  UIImageView+Utils.swift
//  PickUpp
//
//  Created by Артем Кулагин on 11.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func downloadedFrom(url: URL, complete: EmptyBlock? = nil) {
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                complete?()
                self.image = image
            }
            }.resume()
    }
    func downloadedFrom(_ link: String?, complete: EmptyBlock? = nil) {
        guard let link = link, let url = URL(string: link) else {
            self.image = nil
            complete?()
            return
        }
        downloadedFrom(url: url, complete: complete)
    }
}
