//
//  Utils.swift
//  Spytricks
//
//  Created by Артем Кулагин on 09.02.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

typealias EmptyBlock = () -> Void
typealias AliasDictionary = [String: Any]
typealias AliasArrayDictionary = [AliasDictionary]
typealias AliasDictionaryString = [String: String]
typealias BlockObjectsDictionary = (AliasDictionary) -> Void
typealias BlockObjectsDictionaryOptional = (AliasDictionary?) -> Void
typealias AliasInt = (Int) -> Void
typealias AliasIntOptional = (Int?) -> Void
typealias AliasBool = (Bool) -> Void
typealias AliasInt64 = (Int64?) -> Void
typealias AliasString = (String) -> Void

typealias AliasStringOptional = (String?) -> Void
typealias AliasTupleKeyValue = (key: String, value: String)
typealias AliasArrayTupleKeyValue = [AliasTupleKeyValue]
typealias AliasButton = (UIButton) -> Void
typealias AliasError = (NSError?) -> ()

typealias AliasHashable = [AnyHashable : Any]
typealias AliasAttributed = [NSAttributedStringKey : Any]
typealias AliasArrayHashable = [[AnyHashable : Any]]
typealias AliasBlockDate = (NSDate) -> ()
typealias AliasBlockImage = (UIImage) -> ()
typealias AliasBLockCGFloat = (CGFloat) -> ()
typealias AliasFloat = (Float?) -> ()
typealias AliasImagePickerBlock = (UIImagePickerControllerSourceType) -> ()
typealias AliasImageBlock = (UIImage?) -> ()
typealias AliasCLLocation = (CLLocation?) -> ()
typealias AliasCLLocationCoordinate2D = (CLLocationCoordinate2D?) -> ()

typealias AliasViewController = ((UIViewController?) -> ())

func NSLoc(_ key: String?) -> String {
    guard let key = key else {
        return ""
    }
    return NSLocalizedString(key, comment: "")
}

class Utils {
    
    static var isIPad: Bool {
        return UIDevice.current.userInterfaceIdiom == .pad
    }
    
    static var isIphoneX: Bool {
        return UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436
    }
    
    static func stringFromSwiftClass(_ swiftClass: AnyClass) -> String {
        return NSStringFromClass(swiftClass).components(separatedBy: ".").last!
    }
    static func dictToJson(_ name: String?) -> Any? {
        if let path = Bundle.main.url(forResource: name, withExtension: "json") {
            do {
                let data = try Data(contentsOf: path, options: .alwaysMapped)
                do {
                    return try JSONSerialization.jsonObject(with: data, options: [])
                } catch {
                    print(error.localizedDescription)
                }
            } catch let error {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    static func mainQueue(_ handler: @escaping EmptyBlock) {
        DispatchQueue.main.async(execute: handler)
    }
    
    static func queueUserInitiated(handler: EmptyBlock?) {
        queueGlobal(.userInitiated, handler: handler)
    }
    
    static func queueBackground(handler: EmptyBlock?) {
        queueGlobal(.background, handler: handler)
    }
    
    static func queueUtility(handler: EmptyBlock?) {
        queueGlobal(.utility, handler: handler)
    }
    
    static func queueGlobal(_ qos: DispatchQoS.QoSClass = DispatchQoS.QoSClass.default, handler: EmptyBlock?) {
        DispatchQueue.global(qos: qos).async {
            handler?()
        }
    }
    
    static func delay(_ delay:Double, closure: EmptyBlock?) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: {
            closure?()
        })
    }
    
    static func secondsToStr(seconds : Int) -> String {
        let m = seconds / 60
        let s = seconds % 60
        if s < 10 {
            return "\(m):0\(s)"
        } else {
            return "\(m):\(s)"
        }
    }
    
    static func convertObjectsDictionary(objects:[Any], handler:BlockObjectsDictionary?){
        for object in objects {
            guard let object = object as? String else {
                continue
            }
            if let json = convertToDictionary(text: object) {
                handler?(json)
            }
        }
    }
    
    static var versionBundle: String? {
        guard let version = object("CFBundleShortVersionString"),
            let bundle = object("CFBundleVersion") else {
                return nil
        }
        return "Версия : \(version).\(bundle)"
    }
    
    
    static func object(_ key: String) -> Any? {
        return Bundle.main.object(forInfoDictionaryKey: key)
    }
    
    static func convertToDictionary(text:String) -> AliasDictionary? {
        let data = text.data(using: .utf8)
        return data?.jsonObject()
    }
    
    static func openLink(_ link: String?) {
        guard let link = link,
            let url = URL(string: link) else {
            return
        }
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }

}
