//
//  STSizeHelper.swift
//  Spytricks
//
//  Created by Артем Кулагин on 05.05.17.
//  Copyright © 2017 Ivan Alekseev. All rights reserved.
//

import UIKit

let heightBar: Float = 47

let kHeightIPhoneXTopBar: Float = 41
let kHeightDefaultTopBarButtons: Float = 18
let kHeightIpadTopBarButtons: Float = 28

let tapBarHeight: Float = 70

class STSizeHelper {
    
    static var isIPad: Bool {
        return Utils.isIPad
    }
    
    static var isIphoneX: Bool {
        return Utils.isIphoneX
    }
    
    static var heightAllNavBar:Float {
        return heightTopBarButtons + heightBar
    }
    
    static var allHeightNavAndTop: Float {
        return heightBar + STSizeHelper.heightTopBarButtons
    }
    
    static var heightTopBarButtons:Float {
        if isIPad {
            return kHeightIpadTopBarButtons
        } else if isIphoneX {
            return kHeightIPhoneXTopBar
        }else {
            return kHeightDefaultTopBarButtons
        }
    }
}
