//
//  RNSScrollContainer.swift
//  RNIS
//
//  Created by Артем Кулагин on 04.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit
/**
 Класс скролл контейнера
 */
class RNSScrollKeyBoardContainer: UIViewController, KeyboardShowable, UIScrollViewDelegate {
    
    @IBOutlet weak var navTitleLabel: RNSTopTitle!
    /// переменная высоты нижнего констрейнта
    @IBOutlet weak var backButton: RNSLeftBackButton!
    @IBOutlet weak var navBar: PUPCustomBar!
    
    var viewBottomHeightLayoutConstraint: NSLayoutConstraint? {
        get {
            return bottomConstraint
        }
    }
    
    @IBOutlet weak var scrollView: UIScrollView!
    /// переменная для проверки добавления тапа на вью если нужно
    var isNeedAddTap: Bool = true
    var handlerActionTap: EmptyBlock?
    var handlerViewDidLoad: EmptyBlock?
    /// Оутлет переменная содержащая нижний констрейнт
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    /// Метод инициализации контроллера
    static func initController(_ container: UIViewController?, isNeedAddTap: Bool = true) -> RNSScrollKeyBoardContainer? {
        let vc = RNSScrollKeyBoardContainer.initialControllerType()
        vc?.containerVC = container
        vc?.isNeedAddTap = isNeedAddTap
        return vc
    }
    /// Переменная содержащая вью покрытия
    @IBOutlet weak var coverView: UIView!
    var containerVC:UIViewController?
    
    /// Метод инициализации класса
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false;
            scrollView.contentInset = UIEdgeInsetsMake(0,0,0,0);
        }
        
        prepareContainer()
        handlerViewDidLoad?()
    }
 
    func prepareContainer() {
        guard let containerVC = containerVC,
            let containerView = containerVC.view else {
                return
        }
        self.addChildViewController(containerVC)
        coverView.addSubview(containerView)
        containerView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func prepareContentY(_ y: CGFloat) {
        var offset = scrollView.contentOffset
        offset.y = y
        scrollView.setContentOffset(offset, animated: true)
    }
    
    /// метод обработки появления контроллера
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addKeyboardObservers()
    }
    
    /// метод обработки исчезновения контроллера
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeAllObservers()
    }
}
