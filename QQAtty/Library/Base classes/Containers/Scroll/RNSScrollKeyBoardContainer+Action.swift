//
//  RNSScrollKeyBoardContainer+Action.swift
//  PickUpp
//
//  Created by Артем Кулагин on 12.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import Foundation

extension RNSScrollKeyBoardContainer {
    
    @IBAction func actionBack(_ sender: Any) {
        if let handlerActionTap = handlerActionTap {
            handlerActionTap()
        } else {
            STRouter.popMain()
        }
    }
}
