//
//  RNSScrollKeyBoardContainer+Nav.swift
//  PickUpp
//
//  Created by Артем Кулагин on 12.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import Foundation
import UIKit

extension RNSScrollKeyBoardContainer {
    
    func prepareNavigator(_ titleNav: String? = nil) {
        navTitleLabel.isHidden = false
        navBar.isHidden = false
        navTitleLabel.text = titleNav
    }
    
    func prepareBackButton(newIcon: UIImage? = nil, handlerAction: EmptyBlock? = nil) {
        backButton.isHidden = false
        if let newIcon = newIcon {
            backButton.setImage(newIcon, for: UIControlState())
        }
        backButton.touchUpInside(handler: {
            if let handlerAction = handlerAction {
                handlerAction()
            } else {
                STRouter.popMain()
            }
        })
    }
    
    func prepareBackDissmiss() {
        prepareBackButton(newIcon: #imageLiteral(resourceName: "cross")) {
            STRouter.dissmissTop()
        }
    }
}

