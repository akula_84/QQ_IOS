//
//  PUPPopMenuButton.swift
//  PickUpp
//
//  Created by Артем Кулагин on 20.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import UIKit

class PUPPopMenuButton: RNSLeftBackButton {

    override func actionTouch() {
        RNSMenuManager.pop()
    }
}
