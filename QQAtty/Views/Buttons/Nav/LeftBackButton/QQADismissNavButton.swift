//
//  QQADismissNavButton.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQADismissNavButton: RNSLeftBackButton {

    /// Переменная содержащая название картинки
    override var imageShevron: UIImage? {
        return #imageLiteral(resourceName: "cross")
    }
    
    override func actionTouch() {
        STRouter.dissmissTop()
        print("actionTouch QQADismissNavButton")
    }
}
