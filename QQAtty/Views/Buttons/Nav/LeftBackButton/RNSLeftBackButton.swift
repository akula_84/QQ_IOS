//
//  RNSLeftBackButton.swift
//  RNIS
//
//  Created by Артем Кулагин on 03.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit
import SnapKit

/**
 Кнопка возврата на предыдущий контроллер
 */
class RNSLeftBackButton: UIButton {
    
    @IBInspectable var prepareDefaultConstaints: Bool = true
    /// Переменная содержащая левый отступ
    @IBInspectable var leftContentOffset: CGFloat = 0
    /// Переменная запуска дефолтного экшена
    @IBInspectable var actionDefault: Bool = true

    /// метод обработки загрузки вью
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareUI()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        prepareUI()
    }
    
    func prepareUI() {
        prepareIcon(imageShevron)
        prepareDefaultAction()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        prepareConstraint()
    }
    
    func prepareIcon(_ image: UIImage?) {
        let image = image?.withRenderingMode(.alwaysOriginal)
        setImage(image, for: .normal)
    }
    
    /// Переменная содержащая название картинки
    var imageShevron: UIImage? {
        return #imageLiteral(resourceName: "shevronLeft")
    }
    
    /// Настройка обработчика по-умолчанию
    func prepareDefaultAction() {
        guard actionDefault else {
            return
        }
        touchUpInside { [weak self] in
            self?.actionTouch()
        }
    }
    
    /// Обработчик нажатия
    func actionTouch() {
        STRouter.popMain()
    }
    
    /// Настройка ограничителей
    func prepareConstraint() {
        guard let view = superview,
            prepareDefaultConstaints else {
            return
        }
        snp.makeConstraints {
            $0.height.equalTo(51)
            $0.width.equalTo(55)
            $0.left.equalTo(view).offset(-leftContentOffset)
            $0.bottom.equalToSuperview()
        }
    }
}
