//
//  PUPGrayLine.swift
//  PickUpp
//
//  Created by Артем Кулагин on 11.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import UIKit

class PUPGrayLine: UIView {

    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = .colorF4F4F6
    }

}
