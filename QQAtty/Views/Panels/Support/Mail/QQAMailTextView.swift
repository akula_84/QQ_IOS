//
//  QQAMailTextView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAMailTextView: RNSSupportTextView {
    override var isValid:Bool {
        return InputFieldsValidator.isValidEmail(text)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.keyboardType = .emailAddress
    }
 
}
