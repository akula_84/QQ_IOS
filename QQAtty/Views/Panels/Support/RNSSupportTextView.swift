//
//  RNSSupportTextView.swift
//  RNIS
//
//  Created by Артем Кулагин on 16.11.2017.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit
/**
 Класс поле ввода текста в контроллера отправки письма в поддержку
 */
class RNSSupportTextView: BaseViewWithXIBInit {
    
    @IBOutlet weak var leftTextView: NSLayoutConstraint!
    /// UILabel для отображения заголовка
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var textView: SLTTextViewPlaceHolder!
    /// Оутлет переменная содержащая констрейнт
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var galkaImage: UIImageView!
    
    
    @IBInspectable var placeholderKey: String? {
        didSet {
            textView.placeholderKey = placeholderKey
            titleLabel.text = placeholderKey
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textView.handlerDidChange = { [weak self] _ in
            self?.updateHeightTextView()
        }
    }
    
    func updateHeightTextView() {
        let size = self.textView.sizeThatFits(CGSize(width: self.textView.bounds.width, height: CGFloat.greatestFiniteMagnitude))
        self.textViewHeightConstraint.constant = size.height
        updatePlaceHolder()
        checkEmptyGalka()
    }
    
    func updatePlaceHolder() {
        titleLabel.isHidden = text?.isEmpty ?? true
    }
    
    /// Переменная содержащая строку
    var text: String? {
        set {
            textView.text = newValue
            updateHeightTextView()
        }
        get {
            return textView.text
        }
    }
    
    var isHiddenGalka: Bool = false {
        didSet {
            galkaImage.isHidden = isHiddenGalka
        }
    }
    
    var isValid: Bool {
        return !(text?.isEmpty ?? true)
    }
    
    func checkEmptyGalka() {
        isHiddenGalka = !isValid
    }
    
    override var nibNamed:String {
        return "RNSSupportTextView"
    }
}
