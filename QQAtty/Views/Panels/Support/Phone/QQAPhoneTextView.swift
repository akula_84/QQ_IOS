//
//  QQAPhoneTextView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAPhoneTextView: RNSSupportTextView {

    let cross = "+"
    var oldText: String?
    
    override var isValid:Bool {
        return format(text).complete
    }
    
    /// Номер телефона
    var number:String {
        return InputFieldsValidator.strip(text)
    }
    
    override func updateHeightTextView() {
        super.updateHeightTextView()
        textFieldDidChange()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        text = "+7"
        textView.keyboardType = .phonePad
    }

    func textFieldDidChange() {
        print("textFieldDidChange")
        let form = format(text)
        if form.haveFormat {
            let value = form.text
            prepareText(value)
            oldText = value
        }else{
            prepareText(oldText)
        }
        if text?.isEmpty ?? true  {
            prepareText("+7")
        }
        checkEmptyGalka()
    }
    
    func prepareText(_ newValue: String?) {
        textView.text = newValue
    }
    
    /// Валидация текста
    ///
    /// - Parameter string: текст
    /// - Returns: валидный/не валидный
    func validPhoneSymbol(_ string: String) -> Bool {
        var set = CharacterSet(charactersIn: "1234567890")
        if text?.count == 0 {
            set.insert(charactersIn: cross)
        }
        return string.trimmingCharacters(in: set).isEmpty
    }
    
    func format(_ phoneNumber: String?) -> (text: String?, haveFormat: Bool, complete: Bool) {
        return InputFieldsValidator.format(phoneNumber)
    }

}
