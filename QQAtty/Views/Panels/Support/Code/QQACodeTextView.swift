//
//  RNSCodeTextView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class RNSCodeTextView: RNSSupportTextView {

    override func awakeFromNib() {
        super.awakeFromNib()
        textView.keyboardType = .phonePad
        titleLabel.textAlignment = .center
        textView.textAlignment = .center
        leftTextView.constant = 0
        textView.prepareCenterPlaceHolder()
    }
    
    override func checkEmptyGalka() {}
    
}
