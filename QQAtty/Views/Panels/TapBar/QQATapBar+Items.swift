//
//  QQATapBar+Items.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import Foundation

extension QQATapBar {
    
    var menuItems: [MenuItem]? {
        return RNSMenuManager.menuItems
    }
    
    func indexAt(_ item: MenuItem?) -> Int {
        guard let item = item else {
            return 0
        }
        return menuItems?.index(of: item) ?? 0
    }
    
    func updateItems() {
        disableAll()
        enableCurrent()
    }
    
    func disableAll() {
        menuItems?.forEach({ prepareCell($0, enable: false)})
    }
    
    func enableCurrent() {
        prepareCell(RNSMenuManager.currentItem,  enable: true)
    }
    
    func prepareCell(_ item: MenuItem?, enable: Bool) {
        //print("prepareCell")
        guard let item = item else {
            return
        }
        let index = indexAt(item)
        images?.valueAt(index)?.image = enable ? item.imageActive : item.imagePassive
        titles?.valueAt(index)?.textColor = enable ? .color278FE5 : .color8E9DB0
    }
}
