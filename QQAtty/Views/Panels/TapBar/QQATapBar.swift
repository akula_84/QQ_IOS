//
//  QQATapBar.swift
//  QQAtty
//
//  Created by Артем Кулагин on 01.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQATapBar: BaseViewWithXIBInit {
    
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var imgOne: UIImageView!
    @IBOutlet weak var imgTwo: UIImageView!
    @IBOutlet weak var imgThree: UIImageView!
    @IBOutlet weak var imgFour: UIImageView!
    
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet weak var labelThree: UILabel!
    @IBOutlet weak var labelFour: UILabel!
    
    var images: [UIImageView]?
    var titles: [UILabel]?
    
    @IBOutlet weak var centerButton: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        images = [imgOne,imgTwo,imgThree,imgFour]
        titles = [labelOne,labelTwo,labelThree,labelFour]
        
        prepareHandlers()
        updateItems()
        heightConstraint.constant = CGFloat(35 + STSizeHelper.heightTopBarButtons)
    }
    
    func prepareHandlers() {
        RNSMenuManager.handlerUpdateItems = { [weak self] in
            self?.updateItems()
        }
    }
}
