//
//  QQATapBar+Actions.swift
//  QQAtty
//
//  Created by Артем Кулагин on 01.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

extension QQATapBar {
    
    @IBAction func actionItem(_ sender: UIButton) {
        RNSMenuManager.showItem(sender.tag)
    }
    
    @IBAction func actionCenter(_ sender: Any) {
        centerButton.image = #imageLiteral(resourceName: "centerClose")
        let vc = QQASelectActController.initialControllerType()
        vc?.handlerClose = {
            self.centerButton.image = #imageLiteral(resourceName: "CenterBlue")
        }
        vc?.present(false)
    }
}
