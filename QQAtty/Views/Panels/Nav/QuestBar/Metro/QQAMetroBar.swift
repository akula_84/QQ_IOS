//
//  QQAMetroBar.swift
//  QQAtty
//
//  Created by Артем Кулагин on 06.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAMetroBar: QQAQuestBarView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        telef.isHidden = true
        favorButton.isHidden = true
        titleView.text = "Выберите метро"
        textField.placeholder = "Поиск по названию"
    }

}
