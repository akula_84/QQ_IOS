//
//  QQAQuestBar.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAQuestBarView: BaseViewWithXIBInit {
    
    @IBOutlet weak var titleView: UILabel!
    @IBOutlet weak var telef: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var favorButton: UIButton!
    
    var handlerFavor: EmptyBlock?
    
    var isFavorite = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        prepareConstraint()
        updateFavorite()
    }
    
    func updateFavorite() {
        let image = isFavorite ? #imageLiteral(resourceName: "bookmark-white-shape") : #imageLiteral(resourceName: "bookmark-whiteBar")
        favorButton.setImage(image, for: UIControlState())
        titleView.text = isFavorite ? "Избранное" :"Вопросы"
    }

    @IBAction func actionTelef(_ sender: Any) {
        
    }
    
    @IBAction func actionFavor(_ sender: Any) {
        isFavorite = !isFavorite
        updateFavorite()
        handlerFavor?()
    }
    
    func prepareConstraint() {
        guard superview != nil else {
                return
        }
        let heightAdd = Float(52)
        snp.makeConstraints {
            $0.height.equalTo(STSizeHelper.allHeightNavAndTop + heightAdd)
        }
    }
    
    /// Названия xib файла
    override var nibNamed:String {
        return "QQAQuestBarView"
    }
}
