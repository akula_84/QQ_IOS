//
//  QQACityBar.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQACityBar: QQAQuestBarView {

    override func awakeFromNib() {
        super.awakeFromNib()
        favorButton.setImage(#imageLiteral(resourceName: "shevronLeft"), for: UIControlState())
        telef.isHidden = true
        titleView.text = "Выберите город"
        textField.placeholder = "Поиск по городам"
    }
    
    @IBAction override func actionFavor(_ sender: Any) {
        RNSMenuManager.pop()
    }
}
