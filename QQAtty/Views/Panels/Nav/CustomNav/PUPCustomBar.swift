//
//  PUPCustomBar.swift
//  PickUpp
//
//  Created by Артем Кулагин on 10.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import UIKit

class PUPCustomBar: QQABlueGradientView {
    
    @IBInspectable var prepareDefaultConstaints: Bool = true

    override func awakeFromNib() {
        super.awakeFromNib()
        prepareConstraint()
    }
    
    func prepareConstraint() {
        guard superview != nil,
            prepareDefaultConstaints else {
            return
        }
        snp.makeConstraints {
            $0.height.equalTo(STSizeHelper.allHeightNavAndTop)
        }
    }
}
