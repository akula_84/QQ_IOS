//
//  QQAParentGradientView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAParentGradientView: UIView {

    var gradient: CAGradientLayer?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        gradient?.frame = bounds
    }
}
