//
//  QQABlueGradientView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQABlueGradientView: QQAParentGradientView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [UIColor.color81B2E9.cgColor, UIColor.color5695DD.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        layer.insertSublayer(gradient, at: 0)
        self.gradient = gradient
    }
}
