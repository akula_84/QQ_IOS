//
//  QQAVerticalBlueGradientView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAVerticalGradientView: QQAParentGradientView {

    override func awakeFromNib() {
        super.awakeFromNib()
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        let one = UIColor.colorFDFDFD.cgColor
        let two = UIColor.colorF7F7FA.cgColor
        gradient.colors = [two, one, one, two, two, two, two]
        layer.insertSublayer(gradient, at: 0)
        self.gradient = gradient
    }
}
