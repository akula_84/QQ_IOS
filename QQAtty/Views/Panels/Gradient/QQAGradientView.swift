//
//  QQAGradientView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAGradientView: QQAParentGradientView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        let blackColor = UIColor.black.cgColor
        gradient.colors = [UIColor.clear.cgColor, blackColor, blackColor,blackColor,blackColor]
        layer.mask = gradient
        
        self.gradient = gradient
    }
}
