//
//  RNSProfileImageView.swift
//  RNIS
//
//  Created by Артем Кулагин on 03.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit
/**
 Представление для отображения и редактирования аватара пользователя
 */
class RNSProfilePhoto: BaseViewWithXIBInit {

    //@IBInspectable var colorImageView: UIColor?
    /// Оутлет Переменная содержащий картинку
    @IBOutlet weak var imageView: UIImageView!
    /// Оутлет Переменная содержащий картинку
    @IBOutlet weak var backImage: UIImageView!
    
    @IBOutlet weak var plusView: RNSCircle!
    /// Показывает изменялось ли представление
    var isChange = false
     
    /// Показывает имеется ли фото
    var isHavePhoto: Bool {
        return imageView.image != nil
    }
    
    var item: QQAUserItem? {
        didSet {
           updateUI()
        }
    }
    
    let loaderView = LoaderView()
    
    /// метод обработки загрузки вью
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //prepareColorImageView()
        //loadData()
    }
    /*
    /// Настройка представления
    func prepareColorImageView() {
        guard let color = colorImageView else {
            return
        }
        backImage.backgroundColor = color
    }
    */
    /// Событие нажатия на представление
    @IBAction func actionButton(_ sender: Any) {
       showSelectSoure()
    }
   
    func showSelectSoure() {
        let vc = RNSAlertPhotoController.controller(isHavePhoto, complete: showImagePicker, handlerRemove:removePhoto)
        vc.popoverPresentationController?.sourceView = self
        vc.popoverPresentationController?.sourceRect = self.bounds
        STRouter.present(vc)
    }
    
    /// Метод обработки события устновки сабвьювс
    override func layoutSubviews() {
        super.layoutSubviews()

        imageView.layer.cornerRadius =  frame.width/2
        backImage.layer.cornerRadius =  frame.width/2
    }
    
    func showLoader() {
        loaderView.showInView(imageView)
    }
    
    func removeLoader() {
        loaderView.remove()
    }
}
