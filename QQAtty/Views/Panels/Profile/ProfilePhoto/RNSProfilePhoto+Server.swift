//
//  RNSProfilePhoto+SendImage.swift
//  PickUpp
//
//  Created by Артем Кулагин on 11.04.2018.
//  Copyright © 2018 Ilya Inyushin. All rights reserved.
//

import Foundation
import Alamofire

extension RNSProfilePhoto {
    
    func loadData() {
        showLoader()
        /*
        PUPGetUser(complete: { [weak self] item in
            self?.removeLoader()
            self?.item = item
        })
        */
    }
    
    func updateUI() {
        //PUPAuthManager.updateCurrentUser()
        //plusView.isHidden = (item?.isEmptyImage ?? true)
        showLoader()
        imageView.downloadedFrom(item?.avatar) { [weak self] in
            self?.removeLoader()
        }
    }
    
    /// Удаление фото
    func removePhoto() {
        /*
        PUPUserDeletePhoto(complete: { [weak self] item in
            self?.removeLoader()
            self?.item = item
            }, failure: { item in
                STRouter.showError(item)
        })
        */
    }
    
    func loadImageToServer(_ image: UIImage?) {
        /*
        guard let image = image,
            let imageData = UIImageJPEGRepresentation(image, 0.1) else {
                return
        }
        let url = serverAddress + "user/upload_photo"
        
        var headers: HTTPHeaders = [
              "Content-type": "multipart/form-data"
        ]
        if let xcsrToken = UserDefaults.xcsrToken {
            headers[kXCSRFTOKEN] = xcsrToken
        }
        showLoader()
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(imageData, withName: "file", fileName: "image.jpeg", mimeType: "image/jpeg")
        }, usingThreshold: UInt64.init(), to: url, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.response {[weak self] response  in
                    self?.removeLoader()
                    do {
                        let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments)
                        self?.parseError(json as AnyObject)
                    } catch {
                        STRouter.showAlert("error load image")
                    }
                    //self?.parseError(response.result)
                }
            case .failure(let error):
                let error = "requestWith Error in upload: \(error.localizedDescription)"
                STRouter.showAlert(error)
                print(error)
            }
        }
        */
    }
    
    func parseError(_ obj: AnyObject?) {
        /*
        if PUPReplyError(reply: obj)?.checkHaveError() ?? true {
            return
        }
        self.item = PUPUser(reply: obj)
        */
    }
}
