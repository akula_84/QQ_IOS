//
//  QQAProfileEmptyView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 08.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAProfileEmptyView: BaseViewWithXIBInit {
    
    var handlerAction: EmptyBlock?

    @IBAction func actionButton(_ sender: Any) {
        handlerAction?()
    }
}
