//
//  QQAUserView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAUserView: BaseViewWithXIBInit {

    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
  
    var item: QQAQuestItem? {
        didSet {
            updateItem()
        }
    }
    
    func updateItem() {
        authorLabel.text = item?.author
        dateLabel.text = item?.dateText
        cityLabel.text = item?.city
    }
}
