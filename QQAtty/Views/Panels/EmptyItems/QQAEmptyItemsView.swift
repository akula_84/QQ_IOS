//
//  QQAEmptyItemsView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAEmptyItemsView: BaseViewWithXIBInit {
    
    var handlerAction: EmptyBlock?
    
    
    @IBInspectable var image: UIImage? {
        didSet {
            imageView.image = image
        }
    }
    
    @IBInspectable var title: String? {
        didSet {
            label.text = title
        }
    }
    
    @IBInspectable var titleButton: String? {
        didSet {
            button.setTitle(titleButton, for: UIControlState())
        }
    }
    

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var button: UIButton!
    
    @IBAction func actionButton(_ sender: Any) {
        handlerAction?()
    }
}
