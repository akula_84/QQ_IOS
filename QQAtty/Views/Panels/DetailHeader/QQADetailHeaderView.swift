//
//  QQADetailHeaderView.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQADetailHeaderView: BaseViewWithXIBInit {

    @IBOutlet weak var userView: QQAUserView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var askLabel: UILabel!
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var heightTextView: NSLayoutConstraint!
    @IBOutlet weak var buttomView: QQAGradientView!
    
    var handlerUpdateItem: EmptyBlock?
    
    var item: QQAQuestItem? {
        didSet {
            updateItem()
        }
    }
    
    func updateItem() {
        userView.item = item
        titleLabel.text = item?.title
        descLabel.attributedText = item?.descriptionAttr
        askLabel.text =  "ОТВЕТЫ ЮРИСТОВ (\(item?.countAsk ?? 0))"
        buttomView.isHidden = !(item?.isShowOpenButton ?? true)
        updateHeight()
    }
    
    func updateButtonOpen() {
        let title = (item?.openDetail ?? false) ? "Свернуть" : "Развернуть"
        openButton.setTitle(title, for: UIControlState())
        
    }
    
    func updateHeight() {
        updateButtonOpen()
        heightTextView.constant = item?.heightDetail ?? 0
        handlerUpdateItem?()
    }

    @IBAction func openAction(_ sender: Any) {
        item?.revertOpen()
        updateHeight()
    }

}
