//
//  RNSTopTitle.swift
//  RNIS
//
//  Created by Артем Кулагин on 03.08.17.
//  Copyright © 2017 Артем Кулагин. All rights reserved.
//

import UIKit

/**
 Собственная надпись, предназначена для отображения заголовков
 */
class RNSTopTitle: UILabel {

    /// метод обработки загрузки вью
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textColor = .white
        font = .sfMedium17
        textAlignment = .center
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        prepareConstraint()
    }
    
    func prepareConstraint() {
        guard superview != nil else {
            return
        }
        snp.makeConstraints {
            $0.bottom.equalToSuperview().inset(13)
            $0.left.right.equalToSuperview().inset(52)
        }
    }
}
