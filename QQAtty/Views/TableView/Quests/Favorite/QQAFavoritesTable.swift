//
//  QQAFavoritesTable.swift
//  QQAtty
//
//  Created by Артем Кулагин on 10.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAFavoritesTable: QQAQuestsTable {
    
    var emptyView: UIView?
    
    override func awakeFromNib() {
        prepareEmpty()
        super.awakeFromNib()
    }
  
    func prepareEmpty() {
        let view = QQAEmptyFavorView()
        addSubview(view)
        view.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        emptyView = view
    }
    
    override func loadData() {
        STRouter.showLoader()
        Utils.queueUserInitiated {
            let items = QQAStubManager.favorites
            Utils.mainQueue {
                STRouter.removeLoader()
                self.items = items
            }
        }
    }
    
    override func reloadTable() {
        super.reloadTable()
        emptyView?.isHidden = !(items?.isEmpty ?? true)
    }
}
