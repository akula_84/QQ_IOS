//
//  QQADefaultQuestsTable.swift
//  QQAtty
//
//  Created by Артем Кулагин on 09.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQADefaultQuestsTable: QQAQuestsTable {

    override func loadData() {
        STRouter.showLoader()
        Utils.queueUserInitiated {
            let items = QQAQuestItem.stubs
            Utils.mainQueue {
                STRouter.removeLoader()
                self.items = items
            }
        }
    }
}
