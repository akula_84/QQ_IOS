//
//  QQAQuestsTable.swift
//  QQAtty
//
//  Created by Артем Кулагин on 03.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAQuestsTable: BaseViewWithXIBInit {

    @IBOutlet weak var tableView: RNSRegisterTableView!
    
    var items: [QQAQuestItem]? {
        didSet {
            reloadTable()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 0, right: 0)
        loadData()
    }
    
    
    func reloadTable() {
        tableView.reloadData()
    }
    
    func loadData() {}
    
    /// Названия xib файла
    override var nibNamed:String {
        return "QQAQuestsTable"
    }
}
