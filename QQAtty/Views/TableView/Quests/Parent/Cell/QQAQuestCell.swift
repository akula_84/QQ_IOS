//
//  QQAQuestCell.swift
//  QQAtty
//
//  Created by Артем Кулагин on 02.08.2018.
//  Copyright © 2018 Артем Кулагин. All rights reserved.
//

import UIKit

class QQAQuestCell: RNSBaseTableCell {
    
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel! // (html текст) - сокращенный для отображения в ленте
    @IBOutlet var countAskLabel: UILabel!
    @IBOutlet var isFavoriteButton: UIButton!

    var item: QQAQuestItem? {
        didSet {
            updateitem()
        }
    }
    
    func updateitem() {
        titleLabel.text = item?.title
        timeLabel.text = item?.dateText
        descriptionLabel.attributedText = item?.descriptionAttr
        countAskLabel.text = item?.countAskText
        prepareFavorite()
    }
    
    func prepareFavorite() {
        guard let image = item?.isFavoriteImage else {
            return
        }
        isFavoriteButton.setImage(image, for: UIControlState())
    }
    
    @IBAction func favorititeAction(_ sender: Any) {
        //Здесь будет нужна проверка на регистрацию
        item?.revertFavorites()
        prepareFavorite()
        QQAStubManager.prepareFavorites()
    }
}
