//
//  SLTTextViewPlaceHolder.swift
//  Solity-iOS
//
//  Created by Artem Kulagin on 20.09.16.
//  Copyright © 2016 el-machine. All rights reserved.
//

import UIKit

class SLTTextViewPlaceHolder: UITextView,UITextViewDelegate {
    
    typealias DidChangeBlock = (UITextView) -> ()
    
    var placeHolder:UILabel?
    var handlerDidChange: DidChangeBlock?
    
    @IBInspectable var placeholderKey: String? {
        didSet {
            self.preparePlaceholder(placeholderKey)
        }
    }
    
    override var isHidden: Bool {
        didSet{
            if let placeHolder = placeHolder {
                placeHolder.isHidden = self.isHidden
            }
        }
    }
    
    func preparePlaceholder(_ placeholderKey: String?) {
        guard let placeholderKey = placeholderKey else {
            return
        }
        placeHolder?.text = placeholderKey
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        preparePlaceHolder()
        self.delegate = self
    }
    
    fileprivate func preparePlaceHolder() {
        let label = UILabel()
        label.font = font
        label.textColor = .colorA7ACB6
        addSubview(label)
        label.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalToSuperview().offset(4)
            $0.right.equalToSuperview()
        }
        placeHolder = label
    }
    
    func prepareCenterPlaceHolder() {
        placeHolder?.snp.removeConstraints()
        placeHolder?.snp.makeConstraints {
            $0.center.equalToSuperview()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        updatePlaceHolder()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        print("textViewDidChange")
        handlerDidChange?(self)
        updatePlaceHolder()
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textViewDidChange(textView)
    }
    
    func updatePlaceHolder() {
        if let placeHolder = placeHolder {
             placeHolder.isHidden = !text.isEmpty
        }
    }
}
